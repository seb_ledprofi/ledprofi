jQuery(function($) {
$('._sfm_fassung > li > label > .alle_input').click(function(){
    if (this.checked) {
        $('._sfm_fassung > li.cat-item.alle').css('display', 'none')
        $('li.cat-item._GU10').css('display', 'inline-block');
        $('li.cat-item._E27').css('display', 'inline-block');
        $('li.cat-item._E14').css('display', 'inline-block');
        $('.G9').css('display', 'inline-block');
        $('.MR16').css('display', 'inline-block');
        $('._sfm_fassung > li.cat-item > #_sfm_fassung_label').removeClass('selected_fassung');
    }
});
$('._sfm_birnenleistung > li > label > .alle_input').click(function(){
    if (this.checked) {
        $('._sfm_birnenleistung > li.cat-item.alle').css('display', 'none')
        $('._sfm_birnenleistung > li.cat-item > #_sfm_birnenleistung_label').removeClass('selected_fassung');
        $('li.cat-item._25').css('display', 'inline-block');
        $('li.cat-item._35').css('display', 'inline-block');
        $('li.cat-item._40').css('display', 'inline-block');
        $('li.cat-item._50').css('display', 'inline-block');
        $('li.cat-item._60').css('display', 'inline-block');
        $('li.cat-item._75').css('display', 'inline-block');
        $('li.cat-item._100').css('display', 'inline-block');
    }
});
$('._sfm_retro-look > li > label > .alle_input').click(function(){
    if (this.checked) {
        $('._sfm_retro-look > li.cat-item.alle').css('display', 'none')
        $('._sfm_retro-look > li.cat-item > #_sfm_retro-look_label').removeClass('selected_fassung');
        $('._sfm_retro-look > li.cat-item._1').css('display', 'inline-block');
    }
});
$('._sfm_premium > li > label > .alle_input').click(function(){
    if (this.checked) {
        $('._sfm_premium > li.cat-item.alle').css('display', 'none')
        $('._sfm_premium > li.cat-item > #_sfm_premium_label').removeClass('selected_fassung');
        $('._sfm_premium > li.cat-item._1').css('display', 'inline-block');
    }
});

$('._sfm_dimmbar > li > label > .alle_input').click(function(){
    if (this.checked) {
        $('._sfm_dimmbar > li.cat-item.alle').css('display', 'none')
        $('._sfm_dimmbar > li.cat-item > #_sfm_dimmbar_label').removeClass('selected_fassung');
        $('._sfm_dimmbar > li.cat-item._1').css('display', 'inline-block');
    }
});

jQuery( document ).ready(function() {

    if(!$('._sfm_fassung > li > label > .alle_input').is(':checked')) {
        $('._sfm_fassung > li.cat-item.alle').css('display', 'block')
    }
    if(!$('._sfm_birnenleistung > li > label > .alle_input').is(':checked')) {
        $('._sfm_birnenleistung > li.cat-item.alle').css('display', 'block')
    }
    if(!$('._sfm_retro-look > li > label > .alle_input').is(':checked')) {
        $('._sfm_retro-look > li.cat-item.alle').css('display', 'block')
        $('._sfm_retro-look > li.cat-item._1').css('display', 'none')
    }
    if(!$('._sfm_retro-look > li > label > #_sfm_retro-look_input_1').is(':checked')) {
        $('._sfm_retro-look > li.cat-item.alle').css('display', 'none')
        $('._sfm_retro-look > li.cat-item._1').css('display', 'block')
    }
    if(!$('._sfm_premium > li > label > .alle_input').is(':checked')) {
        $('._sfm_premium > li.cat-item.alle').css('display', 'block')
        $('._sfm_premium > li.cat-item._1').css('display', 'none')
    }
    if(!$('._sfm_premium > li > label > #_sfm_premium_input_1').is(':checked')) {
        $('._sfm_premium > li.cat-item.alle').css('display', 'none')
        $('._sfm_premium > li.cat-item._1').css('display', 'block')
    }
    if(!$('._sfm_dimmbar > li > label > .alle_input').is(':checked')) {
        $('._sfm_dimmbar > li.cat-item.alle').css('display', 'block')
        $('._sfm_dimmbar > li.cat-item._1').css('display', 'none')
    }
    if(!$('._sfm_dimmbar > li > label > #_sfm_dimmbar_input_1').is(':checked')) {
        $('._sfm_dimmbar > li.cat-item.alle').css('display', 'none')
        $('._sfm_dimmbar > li.cat-item._1').css('display', 'block')
    }
    if(!$('._sfm_glas > li > label > .alle_input').is(':checked')) {
        $('._sfm_glas > li.cat-item.alle').css('display', 'block')
    }
    if(!$('._sfm_premium > li > label > .alle_input').is(':checked')) {
        $('._sfm_premium > li.cat-item.alle').css('display', 'block')
    }
    if(!$('._sfm_dimmbar > li > label > .alle_input').is(':checked')) {
        $('._sfm_dimmbar > li.cat-item.alle').css('display', 'block')
    }
    if(!$('._sfm_lichtfarbe-beschreibung > li > label > .alle_input').is(':checked')) {
        $('._sfm_lichtfarbe-beschreibung > li.cat-item.alle').css('display', 'block')
    }
    if(!$('._sfm_form > li > label > .alle_input').is(':checked')) {
        $('._sfm_form > li.cat-item.alle').css('display', 'block')
    }
    if(!$('._sfm_spannung_in_volt > li > label > .alle_input').is(':checked')) {
        $('._sfm_spannung_in_volt > li.cat-item.alle').css('display', 'block')
    }
});




$( document ).ready(function() {



$( ".sf-field-post-meta-premium > h4" ).append( "<p style='font-size:12px;font-weight:normal;margin-bottom:4px;'>Beste Farbwiedergabe & Komponenten</p>" );
});

$('._sfm_form > li > label > .alle_input').click(function(){
    if (this.checked) {
        $('._sfm_form > li.cat-item.alle').css('display', 'none')
        $('._sfm_form > li.cat-item > #_sfm_form_label').removeClass('selected_fassung');
    }
});
$('._sfm_lichtfarbe-beschreibung > li > label > .alle_input').click(function(){
    if (this.checked) {
        $('._sfm_lichtfarbe-beschreibung > li.cat-item.alle').css('display', 'none')
        $('._sfm_lichtfarbe-beschreibung > li.cat-item > #_sfm_lichtfarbe-beschreibung_label').removeClass('selected_fassung');
        $('li.cat-item._warmweiß').css('display', 'block');
        $('li.cat-item._naturweiß').css('display', 'block');
        $('li.cat-item._extra').css('display', 'block');
    }
});
$('._sfm_spannung_in_volt > li > label > .alle_input').click(function(){
    if (this.checked) {
        $('._sfm_spannung_in_volt > li.cat-item.alle').css('display', 'none')
        $('._sfm_spannung_in_volt > li.cat-item > #_sfm_spannung_in_volt_label').removeClass('selected_fassung');
        $('._12V').css('display', 'block');
        $('._110-265V').css('display', 'block');
        $('._200-260V').css('display', 'block');
        $('._230V').css('display', 'block');
    }
});
$(document).click(function(){

    if(!$('._sfm_retro-look > li > label > .alle_input').is(':checked')) {
        $('._sfm_retro-look > li.cat-item.alle').css('display', 'block')
        $('._sfm_retro-look > li.cat-item._1').css('display', 'none')
    }
    if(!$('._sfm_retro-look > li > label > #_sfm_retro-look_input_1').is(':checked')) {
        $('._sfm_retro-look > li.cat-item.alle').css('display', 'none')
        $('._sfm_retro-look > li.cat-item._1').css('display', 'block')
    }
    if(!$('._sfm_premium > li > label > .alle_input').is(':checked')) {
        $('._sfm_premium > li.cat-item.alle').css('display', 'block')
        $('._sfm_premium > li.cat-item._1').css('display', 'none')
    }
    if(!$('._sfm_premium > li > label > #_sfm_premium_input_1').is(':checked')) {
        $('._sfm_premium > li.cat-item.alle').css('display', 'none')
        $('._sfm_premium > li.cat-item._1').css('display', 'block')
    }
    if(!$('._sfm_dimmbar > li > label > .alle_input').is(':checked')) {
        $('._sfm_dimmbar > li.cat-item.alle').css('display', 'block')
        $('._sfm_dimmbar > li.cat-item._1').css('display', 'none')
    }
    if(!$('._sfm_dimmbar > li > label > #_sfm_dimmbar_input_1').is(':checked')) {
        $('._sfm_dimmbar > li.cat-item.alle').css('display', 'none')
        $('._sfm_dimmbar > li.cat-item._1').css('display', 'block')
    }
    if(!$('._sfm_fassung > li > label > .alle_input').is(':checked')) {
        $('._sfm_fassung > li.cat-item.alle').css('display', 'block')
    }
    if(!$('._sfm_birnenleistung > li > label > .alle_input').is(':checked')) {
        $('._sfm_birnenleistung > li.cat-item.alle').css('display', 'block')
    }
    if(!$('._sfm_lichtfarbe-beschreibung > li > label > .alle_input').is(':checked')) {
        $('._sfm_lichtfarbe-beschreibung > li.cat-item.alle').css('display', 'block')
    }
    if(!$('._sfm_form > li > label > .alle_input').is(':checked')) {
        $('._sfm_form > li.cat-item.alle').css('display', 'block')
    }
    if(!$('._sfm_spannung_in_volt > li > label > .alle_input').is(':checked')) {
        $('._sfm_spannung_in_volt > li.cat-item.alle').css('display', 'block')
    }
    });


$("#popularity").click(function () {
    $("li.sf-field-sort_order > select").val("");
    $('form.searchandfilter').submit();
});
$("#price").click(function () {
    $("li.sf-field-sort_order > select").val("_sfm__price+asc+num");
    $('form.searchandfilter').submit();
});
$("#lebensdauer").click(function () {
    $("li.sf-field-sort_order > select").val("_sfm_lebensdauer_in_jahren_4h_pro_tag+desc+num");
    $('form.searchandfilter').submit();
});


$('#_sfm_fassung_input_E14').click(function(){
    if (this.checked) {
        $('li.cat-item._GU10').css('display', 'none');
        $('li.cat-item._E27').css('display', 'none');
        $('.G9').css('display', 'none');
        $('.MR16').css('display', 'none');
        $('li.cat-item._E14 > #_sfm_fassung_label').addClass('selected_fassung');
    }
});
$('#_sfm_fassung_input_E27').click(function(){
    if (this.checked) {
        $('li.cat-item._E14').css('display', 'none');
        $('li.cat-item._GU10').css('display', 'none');
        $('.G9').css('display', 'none');
        $('.MR16').css('display', 'none');
        $('li.cat-item._E27 > #_sfm_fassung_label').addClass('selected_fassung');
    }
});
$('.G9 > label > input').click(function(){
    if (this.checked) {
        $('li.cat-item._E14').css('display', 'none');
        $('li.cat-item._GU10').css('display', 'none');
        $('li.cat-item._E27').css('display', 'none');
        $('.MR16').css('display', 'none');
        $('.G9 > #_sfm_fassung_label').addClass('selected_fassung');
    }
});
$('#_sfm_fassung_input_GU10').click(function(){
    if (this.checked) {
        $('li.cat-item._E14').css('display', 'none');
        $('li.cat-item._E27').css('display', 'none');
        $('.G9').css('display', 'none');
        $('.MR16').css('display', 'none');
        $('li.cat-item._GU10 > #_sfm_fassung_label').addClass('selected_fassung');
    }
});
$('.MR16 > label > input').click(function(){
    if (this.checked) {
        $('li.cat-item._E14').css('display', 'none');
        $('li.cat-item._E27').css('display', 'none');
        $('.G9').css('display', 'none');
        $('li.cat-item._GU10').css('display', 'none');
        $('.MR16 > #_sfm_fassung_label').addClass('selected_fassung');
    }
});

$('._sfm_form > li.cat-item > label > input.postform').click(function(){
    if (this.checked) {
        $("._sfm_fassung > li.cat-item.alle > label > input").prop("checked", true);
        $("._sfm_premium > li.cat-item.alle > label > input").prop("checked", true);
        $("._sfm_nur-glas-ausfuehrung > li.cat-item.alle > label > input").prop("checked", true);
        $("._sfm_birnenleistung > li.cat-item.alle > label > input").prop("checked", true);
        $("._sfm_dimmbar > li.cat-item.alle > label > input").prop("checked", true);
        $("._sfm_lichtfarbe-beschreibung > li.cat-item.alle > label > input").prop("checked", true);
        $("._sfm_spannung_in_volt > li.cat-item.alle > label > input").prop("checked", true);


    }
});
$('#_sfm_lichtfarbe-beschreibung_input_warmweiß').click(function(){
    if (this.checked) {
        $('li.cat-item._naturweiß').css('display', 'none');
        $('li.cat-item._extra').css('display', 'none');
    }
});
$('#_sfm_lichtfarbe-beschreibung_input_naturweiß').click(function(){
    if (this.checked) {
        $('li.cat-item._warmweiß').css('display', 'none');
        $('li.cat-item._extra').css('display', 'none');
    }
});
$('li.cat-item._extra > label > ._sfm_lichtfarbe-beschreibung_input').click(function(){
    if (this.checked) {
        $('li.cat-item._warmweiß').css('display', 'none');
        $('li.cat-item._naturweiß').css('display', 'none');
    }
});
$('#_sfm_birnenleistung_input_25').click(function(){
    if (this.checked) {
        $('li.cat-item._35').css('display', 'none');
        $('li.cat-item._40').css('display', 'none');
        $('li.cat-item._50').css('display', 'none');
        $('li.cat-item._60').css('display', 'none');
        $('li.cat-item._75').css('display', 'none');
        $('li.cat-item._100').css('display', 'none');
    }
});
$('#_sfm_birnenleistung_input_35').click(function(){
    if (this.checked) {
        $('li.cat-item._25').css('display', 'none');
        $('li.cat-item._40').css('display', 'none');
        $('li.cat-item._50').css('display', 'none');
        $('li.cat-item._60').css('display', 'none');
        $('li.cat-item._75').css('display', 'none');
        $('li.cat-item._100').css('display', 'none');
    }
});
$('#_sfm_birnenleistung_input_40').click(function(){
    if (this.checked) {
        $('li.cat-item._25').css('display', 'none');
        $('li.cat-item._35').css('display', 'none');
        $('li.cat-item._50').css('display', 'none');
        $('li.cat-item._60').css('display', 'none');
        $('li.cat-item._75').css('display', 'none');
        $('li.cat-item._100').css('display', 'none');
    }
});
$('#_sfm_birnenleistung_input_50').click(function(){
    if (this.checked) {
        $('li.cat-item._25').css('display', 'none');
        $('li.cat-item._35').css('display', 'none');
        $('li.cat-item._40').css('display', 'none');
        $('li.cat-item._60').css('display', 'none');
        $('li.cat-item._75').css('display', 'none');
        $('li.cat-item._100').css('display', 'none');
    }
});
$('#_sfm_birnenleistung_input_60').click(function(){
    if (this.checked) {
        $('li.cat-item._25').css('display', 'none');
        $('li.cat-item._35').css('display', 'none');
        $('li.cat-item._40').css('display', 'none');
        $('li.cat-item._50').css('display', 'none');
        $('li.cat-item._75').css('display', 'none');
        $('li.cat-item._100').css('display', 'none');
    }
});
$('#_sfm_birnenleistung_input_75').click(function(){
    if (this.checked) {
        $('li.cat-item._25').css('display', 'none');
        $('li.cat-item._35').css('display', 'none');
        $('li.cat-item._40').css('display', 'none');
        $('li.cat-item._50').css('display', 'none');
        $('li.cat-item._60').css('display', 'none');
        $('li.cat-item._100').css('display', 'none');
    }
});
$('#_sfm_birnenleistung_input_100').click(function(){
    if (this.checked) {
        $('li.cat-item._25').css('display', 'none');
        $('li.cat-item._35').css('display', 'none');
        $('li.cat-item._40').css('display', 'none');
        $('li.cat-item._50').css('display', 'none');
        $('li.cat-item._60').css('display', 'none');
        $('li.cat-item._75').css('display', 'none');
    }
});
$('._12V > label > ._sfm_spannung_in_volt_input').click(function(){
    if (this.checked) {
        $('._110-265V').css('display', 'none');
        $('._200-260V').css('display', 'none');
        $('._230V').css('display', 'none');
    }
});
$('._110-265V > label > ._sfm_spannung_in_volt_input').click(function(){
    if (this.checked) {
        $('._12V').css('display', 'none');
        $('._200-260V').css('display', 'none');
        $('._230V').css('display', 'none');
    }
});
$('._230V > label > ._sfm_spannung_in_volt_input').click(function(){
    if (this.checked) {
        $('._12V').css('display', 'none');
        $('._110-265V').css('display', 'none');
        $('._200-260V').css('display', 'none');
    }
});
$('._200-260V > label > ._sfm_spannung_in_volt_input').click(function(){
    if (this.checked) {
        $('._12V').css('display', 'none');
        $('._110-265V').css('display', 'none');
        $('._230V').css('display', 'none');
    }
});
})
