<?php
/**
 * File Security Check
 */
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
	</main><!-- #main -->

	<?php get_sidebar( 'subsidiary' ); // Loads the sidebar-primary.php template. ?>

	<footer <?php hybrid_attr( 'footer' ); ?>>
    <div class="yellow_footer">
		<div class="container">
            <div class="row">
                <div class="col-md-4">
                <h5 class="footer_head">Kontakt</h5>
                <p class="footer_content">Kundenhotline: <a href="tel:0043720884134">+43 720 884134</a></p>
                <p class="footer_content">Mo-Fr 09.00-16.00h</p>
                <p class="footer_content"><a href="mailto:office@ledprofi.com">office@ledprofi.com</a></p>
                </div>
                <div class="col-md-4">
                <a href="/versandkosten/" class="footerlink">
                <h5 class="footer_head">Versandbedingungen</h5>
                <p class="footer_content">Österreich: Kostenlos ab 29,00 € - darunter 6,50 €</p>
                <p class="footer_content">Deutschland: Kostenlos ab 59,00 € - darunter 9,50 €</p>
                </a>
                </div>
                <div class="col-md-4">
                <h5 class="footer_head">Newsletter Anmeldung (5€ geschenkt!)</h5>
                <?php echo do_shortcode( '[mc4wp_form id="24018"]' ) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="gray_footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                <h5 class="footer_head">Sicher einkaufen</h5>
                <div class="float_left">
                <img style="max-width:53px;
margin-right:10px;" src="http://www.ledprofi.com/wp-content/uploads/2015/06/TrustedShops-rgb-Siegel_500Hpx-150x150.png">
                </div>
<ul class="footer_content" >
        <li><span class="glyphicon glyphicon-ok"></span> Schnelle Lieferzeiten</li>
        <li><span class="glyphicon glyphicon-ok"></span> Käuferschutz</li>
        <li><span class="glyphicon glyphicon-ok"></span> Datenschutz</li>
    </ul>
                </div>
                <div class="col-md-4">
                <a class="footerlink" href="/bezahlmoeglichkeiten/">
                <h5 class="footer_head">Sichere Zahlungsmittel</h5>
                <img class="payment_providers klarna" src="http://www.ledprofi.com/wp-content/uploads/2015/06/rechnung-klarna.png" alt="Klarna">
                <img class="payment_providers" src="http://www.ledprofi.com/wp-content/uploads/2015/06/logosofort_rgb_1-e1434019850680.png" alt="Sofort Überweisung">
                <img class="payment_providers" src="http://www.ledprofi.com/wp-content/uploads/2015/06/visa-mastercard-paypal-e1434019902530.png" alt="PayPal">
                <p>Vorkasse</p>
                </a>
                </div>
                <div class="col-md-4">
                <h5 class="footer_head">Versandpartner</h5>
                <img src="http://www.ledprofi.com/wp-content/uploads/2015/06/DPD-logo-2-e1434016529240.png" alt="DPD">
                <img src="http://www.ledprofi.com/wp-content/uploads/2015/06/post-at.png" alt="Die Post">
                </div>
            </div>

        </div>
    </div>
    <div class="container">
			<?php get_template_part( 'menu', 'subsidiary' ); // Loads the menu-subsidiary.php template. ?>


				<p class="credit">
                <a href="/impressum/">Impressum</a> | <a href="/agb/">AGB</a> | <a href="/kontakt/">Kontakt</a> | <a href="http://www.ledprofi.com/ueber-uns/">Über uns</a> | <a href="/agb/#datenschutz">Datenschutz</a> | <a href="/widerrufsbestimmungen/">Widerrufsbestimmungen</a><br /> <br/>
                Alle Preise inkl. Mehrwertsteuer zzgl. Versandkosten<br />
                UVP: Unverbindliche Preisempfehlung des Herstellers</p>

                <?php if ( dynamic_sidebar('trustedshops_1') ) : else : endif; ?>
 				<a href="http://geizhals.at" rel="nofollow" >
 				<img src="http://www.ledprofi.com/wp-content/uploads/120x60_Geizhals_Logo_AT.gif" style="margin: 10px auto;display:block;" />
 				</a>
                <p style="margin-top:10px;" class="credit">Copyright &#169; <?php echo date_i18n( 'Y' ); ?></p><!-- .credit -->


		</div><!-- .container -->

	</footer><!-- #footer -->

	<?php wp_footer(); // wp_footer ?>
    
<script type="text/javascript">
    (function () { 
    var _tsid = 'XB95D06C5898B521EB4DCF9DB5B9C996D'; 
    _tsConfig = { 
        'yOffset': '0', /* offset from page bottom */
        'variant': 'reviews', /* text, default, small, reviews, custom, custom_reviews */
        'customElementId': '', /* required for variants custom and custom_reviews */
        'trustcardDirection': '', /* for custom variants: topRight, topLeft, bottomRight, bottomLeft */
        'customBadgeWidth': '', /* for custom variants: 40 - 90 (in pixels) */
        'customBadgeHeight': '', /* for custom variants: 40 - 90 (in pixels) */
        'disableResponsive': 'false', /* deactivate responsive behaviour */
        'disableTrustbadge': 'false' /* deactivate trustbadge */
    };
    var _ts = document.createElement('script');
    _ts.type = 'text/javascript'; 
    _ts.charset = 'utf-8'; 
    _ts.async = true; 
    _ts.src = '//widgets.trustedshops.com/js/' + _tsid + '.js'; 
    var __ts = document.getElementsByTagName('script')[0];
    __ts.parentNode.insertBefore(_ts, __ts);
    })();
</script>

</body>
</html>