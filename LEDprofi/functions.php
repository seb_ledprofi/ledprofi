<?php

/**
* Load child theme sytlesheet
* 
* Set the $deps to 'nudie' to load after the main stylesheet
*/
function child_theme_styles() {

	wp_enqueue_style( 'child-theme-style', get_stylesheet_uri(), array('nudie'), '1.0' );

}

add_action( 'wp_enqueue_scripts', 'child_theme_styles');


add_filter( 'woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text' );    // 2.1 +
 
function woo_archive_custom_cart_button_text() {
 
        return __( 'In den Einkaufswagen', 'woocommerce' );
 
}

global $wooocommerce;
if (is_product()) {
function custom_add_to_cart_redirect() { 
    return 'http://www.ledprofi.com/warenkorb/'; 
}
add_filter( 'woocommerce_add_to_cart_redirect', 'custom_add_to_cart_redirect' );

}

/**
 * woocommerce_package_rates is a 2.1+ hook
 */
add_filter( 'woocommerce_package_rates', 'hide_shipping_when_free_is_available', 10, 2 );
 
/**
 * Hide shipping rates when free shipping is available
 *
 * @param array $rates Array of rates found for the package
 * @param array $package The package array/object being shipped
 * @return array of modified rates
 */

function hide_shipping_when_free_is_available( $rates, $package ) {
 	
 	// Only modify rates if free_shipping is present
  	if ( isset( $rates['free_shipping'] ) ) {
  	
  		// To unset a single rate/method, do the following. This example unsets flat_rate shipping
  		unset( $rates['flat_rate'] );
  		
  		// To unset all methods except for free_shipping, do the following
  		$free_shipping          = $rates['free_shipping'];
  		$rates                  = array();
  		$rates['free_shipping'] = $free_shipping;
	}
	
	return $rates;
}

function lw_woocommerce_gpf_description( $description, $ID ) {
	global $post;
	$save_post = $post;
	$post = get_post( $ID );
	setup_postdata( $post );
	$excerpt = get_field( "description" );
	$post = $save_post;
	return $excerpt;
}
add_filter( 'woocommerce_gpf_description', 'lw_woocommerce_gpf_description', 10, 2 );


// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );

function woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
	?>
	<a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php echo sprintf (_n( '%d item', '%d items', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ); ?> - <?php echo WC()->cart->get_cart_total(); ?></a> 
	<?php
	
	$fragments['a.cart-contents'] = ob_get_clean();
	
	return $fragments;
}


/**
* Set Values
*
*/
$effizienz = get_field( "energielabel" );
$versandkostengrenze = "29,00 €";



add_filter( 'woo_breadcrumbs_trail', 'woo_custom_breadcrumbs_trail_add_product_categories', 10 );


function woo_get_term_parents( $id, $taxonomy, $link = false, $separator = '/', $nicename = false, $visited = array() ) {
  $chain = '';
  $parent = &get_term( $id, $taxonomy );

  if ( is_wp_error( $parent ) )
    return $parent;

  if ( $nicename )
    $name = $parent->slug;
  else
    $name = $parent->name;


  if ( $parent->parent && ( $parent->parent != $parent->term_id ) && ! in_array( $parent->parent, $visited ) && ! in_array( $parent->term_id, $visited ) ) {
    $visited[] = $parent->parent;
    $visited[] = $parent->term_id;
    $chain .= woo_get_term_parents( $parent->parent, $taxonomy, $link, $separator, $nicename, $visited );
  }

  if ( $link ) {
    $chain .= '<a href="' . get_term_link( $parent, $taxonomy ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $parent->name ) ) . '">'.$name.'</a>' . $separator;
  } else {
    $chain .= $name.$separator;
  }
  
  return $chain;
} // End woo_get_term_parents()
 
add_filter( 'woocommerce_available_shipping_methods', 'remove_standard_shippings_when_free' , 10, 1 );
 
function remove_standard_shippings_when_free( $available_methods ) {
 
    //get cart total from session
    $total = 0;
    $session_cart = $woocommerce->session->cart;
    if(count($session_cart) > 0) {
        foreach($session_cart as $cart_product)
            $total = $total + $cart_product['line_subtotal'];
    }
 
    if( isset( $available_methods['free_shipping'] ) ) {
        // remove all standard shipping options
        unset( $available_methods['flat_rate'] );
        unset( $available_methods['international_delivery'] );
        unset( $available_methods['local_delivery'] );
        unset( $available_methods['local_pickup'] );
    }
 
 return $available_methods;
}


/**
 * Retrieve term parents with separator.
 *
 * @param int $id Term ID.
 * @param string $taxonomy.
 * @param bool $link Optional, default is false. Whether to format with link.
 * @param string $separator Optional, default is '/'. How to separate terms.
 * @param bool $nicename Optional, default is false. Whether to use nice name for display.
 * @param array $visited Optional. Already linked to terms to prevent duplicates.
 * @return string
 */
 


 
if ( ! function_exists( 'woo_get_term_parents' ) ) {
function woo_get_term_parents( $id, $taxonomy, $link = false, $separator = '/', $nicename = false, $visited = array() ) {
	$chain = '';
	$parent = &get_term( $id, $taxonomy );
	if ( is_wp_error( $parent ) )
		return $parent;
 
	if ( $nicename ) {
		$name = $parent->slug;
	} else {
		$name = $parent->name;
	}
 
	if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {
		$visited[] = $parent->parent;
		$chain .= woo_get_term_parents( $parent->parent, $taxonomy, $link, $separator, $nicename, $visited );
	}
 
	if ( $link ) {
		$chain .= '<a href="' . get_term_link( $parent, $taxonomy ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $parent->name ) ) . '">'.$parent->name.'</a>' . $separator;
	} else {
		$chain .= $name.$separator;
	}
	return $chain;
} // End woo_get_term_parents()
}



/**
 * Register Flash Sale Widget Area
 *
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'trustedshops_1',
		'id'            => 'trustedshops_1',
		'before_widget' => '<div class="trustedshops_1">',
		'after_widget'  => '</div>',

	) );
	register_sidebar( array(
		'name'          => 'trustedshops_2',
		'id'            => 'trustedshops_2',
		'before_widget' => '<div class="trustedshops_2">',
		'after_widget'  => '</div>',

	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_to_cart', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_in_stock', 30 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_details_above_fold', 40 );
add_action( 'woocommerce_single_product_bundles', 'woocommerce_bundle', 40 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 00 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_contentfield', 05 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_gzd_template_single_legal_info', wc_gzd_get_hook_priority( 'single_legal_info' ) );
add_action( 'woocommerce_single_product_to_cart', 'woocommerce_total_product_price', 29 );
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );


function woocommerce_total_product_price() {
    global $woocommerce, $product;
    $stueckzahl = get_field("stueckzahl");
    echo sprintf('<div id="product_total_price" style="display:block">%s %s</div>',__('','woocommerce'),'<span class="price">'.$product->get_price_html().'</span>');
    ?>
    <span class="zzgl">
	<button class="infolink" rel="clickover"  data-placement="left" data-toggle="popover" data-content="Unter einem Warenwert von 29,00 € (AT) bzw. 59,00 € (DE) verrechnen wir 6,50 € (AT) bzw. 9,50 € (DE) Versandkosten inkl. Mehrwertsteuer." title="Versandkosten">

				Zzgl. Versandkosten<sup><span class="glyphicon glyphicon-info-sign"></span></sup></button>
    <br /><span style="font-size:11px;">
    <?php $coming_soon_1 = get_field("coming_soon_1");$coming_soon_2 = get_field("coming_soon_2");$verfugbar = get_field("verfugbar"); if ($verfugbar && $product->is_type( 'simple' )) { ?>Geliefert in 1-2 Werktagen.
 <?php }
if ($coming_soon_1 && $product->is_type( 'simple' )) {?>Lieferung innerhalb von 1-2 Wochen
<?php }
if ($coming_soon_2 && $product->is_type( 'simple' )) {?>
Lieferung innerhalb von 6-8 Wochen
<?php }
if ($coming_soon_2 == FALSE && $coming_soon_1 == FALSE && $verfugbar == FALSE && $product->is_type( 'simple' )) {?>
Leider ist dieses Produkt nicht erhältlich.
<?php } ?></span></span>
    <span id="vk_frei" class="versandkostenfrei">Versandkostenfrei<br />
    <span style="font-size:11px;">
<?php $coming_soon_1 = get_field("coming_soon_1");$coming_soon_2 = get_field("coming_soon_2");$verfugbar = get_field("verfugbar"); if ($verfugbar && $product->is_type( 'simple' )) { ?>Geliefert in 1-2 Werktagen.
 <?php }
if ($coming_soon_1 && $product->is_type( 'simple' )) {?>Lieferung innerhalb von 1-2 Wochen
<?php }
if ($coming_soon_2 && $product->is_type( 'simple' )) {?>
Lieferung innerhalb von 6-8 Wochen
<?php }
if ($product->is_type( 'simple' ) && $coming_soon_2 == FALSE && $coming_soon_1 == FALSE && $verfugbar == FALSE) {?>
Leider ist dieses Produkt nicht erhältlich.
<?php } ?>

    </span></span>
    <?php
        if( $product->is_type( 'bundle' )) {?>
        <span class="totalmenge">Stückzahl: <?php echo $stueckzahl ?></span><?php
	} ?>
        <script>
            jQuery(function($){
                var price = <?php $brutto = $product->get_price()*1.2;echo $brutto; ?>,
                    current_cart_total = <?php echo $woocommerce->cart->cart_contents_total; ?>,
                    currency = '<?php echo get_woocommerce_currency_symbol(); ?>';
 

 				$('[name=quantity]').ready(function() {
                    
                        var product_total = parseFloat(price * 1),
                        cart_total = parseFloat(product_total + current_cart_total);
 
                        $('#product_total_price .price').html( product_total.toFixed(2).replace(".", ",") + ' ' + currency );
                    
                    $('#product_total_price').toggle(!(this.value <= 0));

                <?php global $woocommerce; $user_country = $woocommerce->customer->get_country( ); if ($user_country == 'DE') { ?>
 					if (cart_total < 59) {
						$(".zzgl").css("display", "block");
						$("#vk_frei").css("display", "none");
						$(".zzgl_2").css("display", "block");
						$(".versandkostenfrei_2").css("display", "none");

					} 
					else {
						$(".zzgl").css("display", "none");
						$(".versandkostenfrei").css("display", "block");
						$(".zzgl_2").css("display", "none");
						$(".versandkostenfrei_2").css("display", "inline");

					}
				<?php }
				else {
				 ?>
 					if (cart_total < 29) {
						$(".zzgl").css("display", "block");
						$("#vk_frei").css("display", "none");
						$(".zzgl_2").css("display", "block");
						$(".versandkostenfrei_2").css("display", "none");

					} 
					else {
						$(".zzgl").css("display", "none");
						$(".versandkostenfrei").css("display", "block");
						$(".zzgl_2").css("display", "none");
						$(".versandkostenfrei_2").css("display", "inline");

					}
				<?php } ?>
				});


                $('[name=quantity]').change(function(){
                    if (!(this.value < 1)) {
                        var product_total = parseFloat(price * this.value),
                        cart_total = parseFloat(product_total + current_cart_total);
 
                        $('#product_total_price .price').html( product_total.toFixed(2).replace(".", ",") + ' ' + currency );

                		var stueckzahl = <?php echo $stueckzahl; ?>,
                        totale_menge = parseFloat(stueckzahl*this.value);
                        $('.totalmenge').html( 'Stückzahl: ' + totale_menge.toFixed(0).replace(".", ","));
                    }
                    $('#product_total_price').toggle(!(this.value <= 0));
                <?php global $woocommerce; $user_country = $woocommerce->customer->get_country( ); if ($user_country == 'DE') { ?>
 					if (cart_total < 59) {
						$(".zzgl").css("display", "block");
						$("#vk_frei").css("display", "none");
						$(".zzgl_2").css("display", "block");
						$(".versandkostenfrei_2").css("display", "none");

					} 
					else {
						$(".zzgl").css("display", "none");
						$(".versandkostenfrei").css("display", "block");
						$(".zzgl_2").css("display", "none");
						$(".versandkostenfrei_2").css("display", "inline");

					}
				<?php }
				else {
				 ?>
 					if (cart_total < 29) {
						$(".zzgl").css("display", "block");
						$("#vk_frei").css("display", "none");
						$(".zzgl_2").css("display", "block");
						$(".versandkostenfrei_2").css("display", "none");

					} 
					else {
						$(".zzgl").css("display", "none");
						$(".versandkostenfrei").css("display", "block");
						$(".zzgl_2").css("display", "none");
						$(".versandkostenfrei_2").css("display", "inline");

					}
				<?php } ?>
				
                });



            });
        </script>
    <?php
}

/**
* WooCommerce Extra Feature
* --------------------------
*
* Change number of related products on product page
* Set your own value for 'posts_per_page'
*
*/// Redefine wooCommerce related products
 
function woocommerce_output_related_products() {
woocommerce_related_products(4,1); // Display 3 products in rows of 3
}
 
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_upsells', 15 );
 
    if ( ! function_exists( 'woocommerce_output_upsells' ) ) {
        function woocommerce_output_upsells() {
        woocommerce_upsell_display( 4,1 ); // Display 3 products in 1 row
    }
}
/** Change number of upsells in single prodict posts
*  Set the first number to how many total and the second number to how many rows.
*/
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_upsells', 15 );
 
    if ( ! function_exists( 'woocommerce_output_upsells' ) ) {
        function woocommerce_output_upsells() {
        woocommerce_upsell_display(4,1 ); // Display 3 products in 1 row
    }
}


function woocommerce_output_contentfield() {?>
<?php $p_cat = $product->get_categories; if ($p_cat != "Zubehör" && $form != "G9 Leuchtmittel") {?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h2 class="panel-title">Weitere Informationen zu <?php echo get_the_title(); ?></h2>
	</div>
	<div class="panel-body">
	<div class="row">
	<div class="col-md-4">
	<h4>1. 100%-Retro-Fit-Sockel - <?php $sockel = get_field("fassung"); echo $sockel; ?></h4>

<p>Diese Lampe passt garantiert in Ihre vorhandene Fassung - oder 30 Tage Geld zurück!</p>
<p>Testen Sie unsere Lampen sorgenfrei und überzeugen Sie sich von der überragenden Lichtqualität und niedrigem Verbrauch.</p>

<p><?php $prefix_big = get_field("prefix_bezeichnung_gross"); echo $prefix_big; ?> <?php $dimmbar = get_field( "dimmbar" ); if ( $dimmbar ) { ?>dimmbare <?php } ?><?php $hersteller = get_field ("hersteller"); echo $hersteller ?> <?php $form = get_field( "form" ); echo $form?> wurde konstruiert, um Ihre vorhandene Lampe zu ersetzen. Bitte achten Sie auf die korrekte Bezeichnung der Fassung bzw. des Sockels. Bei Fragen schreiben Sie uns eine<br /><a target="_blank" href="http://www.ledprofi.com/kontakt/"><span class="glyphicon glyphicon-envelope"></span> Nachricht</a>!</p>
	</div>
	<div class="col-md-4">
	<?php $form = get_field( "form" ); if ($form == "Glühbirne" or $form == "Mini ILLU Birne") {?>
	<span class="first_info_birne">1.</span>
	<span class="second_info_birne">2.</span>
	<?php } ?>
	<?php if ($form == "Globe") {?>
	<span class="first_info_globe">1.</span>
	<span class="second_info_globe">2.</span>
	<?php } ?>
	<?php if ($form == "Spot") {?>
	<span class="first_info_spot">1.</span>
	<span class="second_info_spot">2.</span>
	<?php } ?>
	<?php if ($form == "Kerzenlampe") {?>
	<span class="first_info_kerze">1.</span>
	<span class="second_info_kerze">2.</span>
	<?php } ?>
	<?php if ($form == "G9 Leuchtmittel"){?>
	<span class="first_info_g9">1.</span>
	<span class="second_info_g9">2.</span>
	<?php } ?>
	<img class="showcase" src="<?php $image_link  	= wp_get_attachment_url( get_post_thumbnail_id() ); echo $image_link; ?>" />
	</div>
	<div class="col-md-4">
	<?php $filament = get_field("filament");
	if ($filament) { ?>
	<h4>2. Filament-Technik</h4>

<p>Die LED Filament Technik gilt als die modernste &amp; beste Art eine LED Lampe mit möglichst naturgetreuem Licht zu produzieren. Gut zu erkennen sind Filament Lampen an den gelben LED Glühfäden, die der Glühbirne nachempfundenen sind.</p>

<p>Sie garantieren optimale Lichtabstrahlung in einem Winkel von <?php $abstrahlwinkel = get_field("abstrahlwinkel"); echo $abstrahlwinkel; ?>° und perfekte Farbwiedergabe von mindestens RA>80, was einer herkömmlichen Halogen- oder Glühbirne am nächsten kommt! Dabei erzeugt diese LED Birne wenig Hitze und verbraucht nur ca. 10% der bisher notwendigen Energie.</p>
<?php };
	$form = get_field("form");
	if ($form == "Spot") { ?>
	<h4>2. <?php $ausfuehrung = get_field("ausfuehrung"); echo $ausfuehrung; ?> &amp; "COB" Technik</h4>

<p>Der Einsatz von moderner Chip-on-Board (kurz COB) Technologie ermöglicht höchste Lichtintensität bei minmalem Verbrauch und geringer Wärmeentwicklung.</p>

<p><?php $hersteller = get_field ("hersteller"); echo $hersteller ?> entwickelt sämtliche LED-Lampen in Österreich und setzt dabei auf hochwertige Komponenten für eine lange Lebensdauer &amp; beste Farbwiedergabe.</p>
	<?php }
	 $filament = get_field("filament");
	if ($filament == FALSE && $form != "Spot") { ?>
		<h4>2. Hohe Lichtqualität</h4>

<p><?php $hersteller = get_field ("hersteller"); echo $hersteller ?> entwickelt sämtliche LED-Lampen in Österreich und setzt dabei auf hochwertige Komponenten für eine lange Lebensdauer &amp; beste Farbwiedergabe.</p>

<p>Mit dem Ergebnis nicht zufrieden? Sie können die Lampe 30 Tage lang ohne Angabe von Gründen zurückschicken.</p>
	<?php }
?>


	</div>
	</div>

</div>
</div>
<?php } ?>
<div class="row equal">
<div class="col-md-7 col-xs-12">
<div class="panel panel-default">
	<div class="panel-heading">
		<h2 class="panel-title">Besser als das Original</h2>
	</div>
	<div class="panel-body">
			<p><?php $prefix_big = get_field("prefix_bezeichnung_gross"); echo $prefix_big; ?> <?php $dimmbar = get_field( "dimmbar" ); if ( $dimmbar ) { ?>dimmbare <?php } ?><?php $hersteller = get_field ("hersteller"); echo $hersteller ?> <?php $form = get_field( "form" ); echo $form?> ersetzt <?php $birnenleistung = get_field("birnenleistung"); echo $birnenleistung?> Watt und ist der erste vollwertige Ersatz für herkömmliche Halogen- oder Glühbirnen!</p>
<p><?php $prefix_big = get_field("prefix_bezeichnung_gross"); echo $prefix_big; ?> <?php $form = get_field("form"); echo $form; ?> wird in Österreich konstruiert und in China bei Vertragspartnern unter strenger Qualitätskontrolle produziert. <?php $prefix_big = get_field("prefix_bezeichnung_gross"); echo $prefix_big; ?> <?php $bezeichnung = get_field("bezeichnung_1"); echo $bezeichnung?> erzeugt eine perfekte Lichttemperatur und maximale Helligkeit bei minimalem Stromverbrauch.</p><p>Sie sparen Geld &amp; müssen erst wieder in ca. <?php $lifetime = get_field( "lebensdauer_in_jahren_4h_pro_tag" ); echo number_format((float)$lifetime, 0, ',', '');?> Jahren auswechseln (bei 4 Stunden Betrieb pro Tag)!</p>
	<p>soft-LED Produkte werden von Kunden verwendet, bei denen beste Qualität sowohl in Verarbeitung als auch Lichtfarbe oberste Priorität hat.</p>
</div>
</div>
</div>
<div class="col-md-5 col-xs-12">
<?php $kerzenlicht = get_field ("kerzenlicht"); $dimmbar = get_field( "dimmbar" ); $premium = get_field("premium"); $flackerfrei = get_field("flackerfrei"); $form = get_field("form"); if ($dimmbar or $premium && $flackerfrei == FALSE && $kerzenlicht == FALSE && $form != "Spot") {?>
		<div class="panel panel-default">
	<div class="panel-heading">
		<h2 class="panel-title">soft-LED Premium Referenzen</h2>
	</div>
	<div class="panel-body">
	<div class="col-md-6">
	<ul class="references">
	<li>
		Wiener Staatsoper
	</li>
	<li>
		Schloss Schönbrunn
	</li>
	<li>
		Volkstheater - Wien
	</li>
	<li>
		Schauspielhaus Graz
	</li>
	<li>
		Staatstheater Nürnberg
	</li>
	</ul>
	</div>
	<div class="col-md-6">
	<ul class="references">
	<li>
		Hofburg - Wien
	</li>
	<li>
		Parlament - Wien
	</li>
	<li>
		Landestheater Salzburg
	</li>
	<li>
		Beyler Moschee - Baku
	</li>
	</ul>
	</div>
	</div>
	</div>
<?php } 
if ($flackerfrei && $kerzenlicht == FALSE) {?>
		<div class="panel panel-default">
	<div class="panel-heading">
		<h2 class="panel-title">soft-LED Premium Referenzen</h2>
	</div>
	<div class="panel-body">
	<div class="col-md-6">
	<ul class="references">
	<li>
		Wiener Staatsoper
	</li>
	<li>
		Schloss Schönbrunn
	</li>
	<li>
		Volkstheater - Wien
	</li>
	<li>
		Schauspielhaus Graz
	</li>
	<li>
		Staatstheater Nürnberg
	</li>
	</ul>
	</div>
	<div class="col-md-6">
	<ul class="references">
	<li>
		Hofburg - Wien
	</li>
	<li>
		Parlament - Wien
	</li>
	<li>
		Landestheater Salzburg
	</li>
	<li>
		Beyler Moschee - Baku
	</li>
	</ul>
	</div>
	</div>
	</div>
<?php } 
if ($kerzenlicht) {?>
		<div class="panel panel-default">
	<div class="panel-heading">
		<h2 class="panel-title">soft-LED Premium Referenzen</h2>
	</div>
	<div class="panel-body">
	<div class="col-md-6">
	<ul class="references">
	<li>
		Wiener Staatsoper
	</li>
	<li>
		Schloss Schönbrunn
	</li>
	<li>
		Volkstheater - Wien
	</li>
	<li>
		Schauspielhaus Graz
	</li>
	<li>
		Staatstheater Nürnberg
	</li>
	</ul>
	</div>
	<div class="col-md-6">
	<ul class="references">
	<li>
		Hofburg - Wien
	</li>
	<li>
		Parlament - Wien
	</li>
	<li>
		Landestheater Salzburg
	</li>
	<li>
		Beyler Moschee - Baku
	</li>
	</ul>
	</div>
	</div>
	</div>
	<?php } 
if ($dimmbar == FALSE && $premium == FALSE && $flackerfrei == FALSE && $kerzenlicht == FALSE && $form == "Spot") {?>
		<div class="panel panel-default">
	<div class="panel-heading">
		<h2 class="panel-title">soft-LED Premium Referenzen</h2>
	</div>
	<div class="panel-body">
	<div class="col-md-6">
	<ul class="references">
	<li>
		Wiener Staatsoper
	</li>
	<li>
		Schloss Schönbrunn
	</li>
	<li>
		Volkstheater - Wien
	</li>
	<li>
		Schauspielhaus Graz
	</li>
	<li>
		Staatstheater Nürnberg
	</li>
	</ul>
	</div>
	<div class="col-md-6">
	<ul class="references">
	<li>
		Hofburg - Wien
	</li>
	<li>
		Parlament - Wien
	</li>
	<li>
		Landestheater Salzburg
	</li>
	<li>
		Beyler Moschee - Baku
	</li>
	</ul>
	</div>
	</div>
	</div>
	<?php } 
if ($dimmbar == FALSE && $premium == FALSE && $flackerfrei == FALSE && $kerzenlicht == FALSE && $form == "Globe") {?>
		<div class="panel panel-default">
	<div class="panel-heading">
		<h2 class="panel-title">soft-LED Premium Referenzen</h2>
	</div>
	<div class="panel-body">
	<div class="col-md-6">
	<ul class="references">
	<li>
		Wiener Staatsoper
	</li>
	<li>
		Schloss Schönbrunn
	</li>
	<li>
		Volkstheater - Wien
	</li>
	<li>
		Schauspielhaus Graz
	</li>
	<li>
		Staatstheater Nürnberg
	</li>
	</ul>
	</div>
	<div class="col-md-6">
	<ul class="references">
	<li>
		Hofburg - Wien
	</li>
	<li>
		Parlament - Wien
	</li>
	<li>
		Landestheater Salzburg
	</li>
	<li>
		Beyler Moschee - Baku
	</li>
	</ul>
	</div>
	</div>
	</div>
	<?php } 
if ($dimmbar == FALSE && $premium == FALSE && $flackerfrei == FALSE && $kerzenlicht == FALSE && $form == "Glühbirne" or $form =="Mini ILLU Birne") {?>
		<div class="panel panel-default">
	<div class="panel-heading">
		<h2 class="panel-title">soft-LED Premium Referenzen</h2>
	</div>
	<div class="panel-body">
	<div class="col-md-6">
	<ul class="references">
	<li>
		Wiener Staatsoper
	</li>
	<li>
		Schloss Schönbrunn
	</li>
	<li>
		Volkstheater - Wien
	</li>
	<li>
		Schauspielhaus Graz
	</li>
	<li>
		Staatstheater Nürnberg
	</li>
	</ul>
	</div>
	<div class="col-md-6">
	<ul class="references">
	<li>
		Hofburg - Wien
	</li>
	<li>
		Parlament - Wien
	</li>
	<li>
		Landestheater Salzburg
	</li>
	<li>
		Beyler Moschee - Baku
	</li>
	</ul>
	</div>
	</div>
	</div>
	<?php } 
if ($dimmbar == FALSE && $premium == FALSE && $flackerfrei == FALSE && $kerzenlicht == FALSE && $form == "Kerzenlampe") {?>
		<div class="panel panel-default">
	<div class="panel-heading">
		<h2 class="panel-title">soft-LED Premium Referenzen</h2>
	</div>
	<div class="panel-body">
	<div class="col-md-6">
	<ul class="references">
	<li>
		Wiener Staatsoper
	</li>
	<li>
		Schloss Schönbrunn
	</li>
	<li>
		Volkstheater - Wien
	</li>
	<li>
		Schauspielhaus Graz
	</li>
	<li>
		Staatstheater Nürnberg
	</li>
	</ul>
	</div>
	<div class="col-md-6">
	<ul class="references">
	<li>
		Hofburg - Wien
	</li>
	<li>
		Parlament - Wien
	</li>
	<li>
		Landestheater Salzburg
	</li>
	<li>
		Beyler Moschee - Baku
	</li>
	</ul>
	</div>
	</div>
	</div>
		<?php } 
if ($dimmbar == FALSE && $premium == FALSE && $flackerfrei == FALSE && $kerzenlicht == FALSE && $form == "G9 Leuchtmittel") {?>
		<div class="panel panel-default">
	<div class="panel-heading">
		<h2 class="panel-title">soft-LED Premium Referenzen</h2>
	</div>
	<div class="panel-body">
	<div class="col-md-6">
	<ul class="references">
	<li>
		Wiener Staatsoper
	</li>
	<li>
		Schloss Schönbrunn
	</li>
	<li>
		Volkstheater - Wien
	</li>
	<li>
		Schauspielhaus Graz
	</li>
	<li>
		Staatstheater Nürnberg
	</li>
	</ul>
	</div>
	<div class="col-md-6">
	<ul class="references">
	<li>
		Hofburg - Wien
	</li>
	<li>
		Parlament - Wien
	</li>
	<li>
		Landestheater Salzburg
	</li>
	<li>
		Beyler Moschee - Baku
	</li>
	</ul>
	</div>
	</div>
	</div><?php } ?>

</div>
</div>


<div class="row equal">
<div class="col-md-7">
<div class="panel panel-default">
	<div class="panel-heading">
		<h2 class="panel-title">Technische Informationen<br /><span style="font-weight:normal;padding-top:6px;margin-top:4px;border-top:1px #ddd dashed;font-weight:normal;display:block;"><?php echo get_the_title(); ?></span></h2>
	</div>
	<div class="panel-body">
<table class="table table-striped">

	
					<tbody>
			<tr class="">
				<th>Hersteller</th>
				<td class="product_weight">soft-LED</td>
			</tr>
					<tr class="alt">
				<th>Bestellnummer</th>
				<td class="product_weight"><?php global $product; echo $product->get_sku(); ?></td>
			</tr>
		
					<tr class="">
				<th>Betriebsspannung</th>
				<td class="product_dimensions"><?php $spannung = get_field( "spannung_in_volt" ); echo $spannung ?></td>
			</tr>
					<tr class="alt">
				<th>Sockelbeschreibung</th>
				<td class="product_weight"><?php $fassung = get_field( "fassung_beschreibung" ); echo $fassung ?></td>
			</tr>
		
					<tr class="">
				<th>Glas</th>
				<td class="product_dimensions"><?php $glas = get_field( "glas" ); echo $glas ?></td>
			</tr>
					<tr class="alt">
				<th>Einschaltzeit</th>
				<td class="product_weight">sofort - ohne Verzögerung (weniger als 250 Millisekunden)</td>
			</tr>
					<tr class="">
				<th>Leistungaufnahme</th>
				<td class="product_weight"><?php $leistungsaufnahme = get_field( "leistungsaufnahme_watt_genau" ); echo $leistungsaufnahme ?> Watt</td>
			</tr>
					<tr class="alt">
				<th>Äquivalenzleistung</th>
				<td class="product_dimensions"><?php $birnenleistung = get_field( "birnenleistung" ); echo $birnenleistung ?> Watt</td>
			</tr>
					<tr class="">
				<th>Lebensdauer</th>
				<td class="product_weight"><button class="infolink" rel="clickover"  data-placement="right" data-toggle="popover" data-content="Diese Lampe funktioniert auch noch nach <?php $lebensdauer_in_stunden = get_field( "lebensdauer_in_stunden" ); echo number_format($lebensdauer_in_stunden, 0, ',', '.'); ?> Stunden, allerdings mit nur noch 70% der Lichtleistung - dh. Sie können diese Lampe sogar noch länger einsetzen!" title="Lebensdauer<a class='close'>&times;</a>"><?php $lebensdauer_in_stunden = get_field( "lebensdauer_in_stunden" ); echo number_format($lebensdauer_in_stunden, 0, ',', '.'); ?> Stunden (L70)<sup><span class="glyphicon glyphicon-info-sign"></span></sup></button></td>
			</tr>
		
					<tr class="alt">
				<th>Lichtleistung</th>
				<td class="product_dimensions"><?php $lumen = get_field( "lumen_l70" ); echo $lumen ?> Lumen</td>
			</tr>
				<tr class="">
				<th>Abstrahlwinkel</th>
				<td class="product_weight"><?php $abstrahlwinkel = get_field( "abstrahlwinkel" ); echo $abstrahlwinkel ?>&deg;</td>
			</tr>
		
					<tr class="alt">
				<th>Dimmfähigkeit</th>
				<td class="product_dimensions">
				<?php 
				$dimmfaehigkeit_beschreibung = get_field( "dimmfaehigkeit_beschreibung" );
				if ($dimmfaehigkeit_beschreibung == 'Nur mit softLED-Systemdimmern dimmbar!') {?>
				<button class="infolink" rel="clickover"  data-placement="top" data-toggle="popover" data-content="Dimmfähig nur in kombination mit soft-LED Systemdimmern" title="Dimmfähigkeit<a class='close'>&times;</a>">

				<?php echo $dimmfaehigkeit_beschreibung ?>
				<sup><span class="glyphicon glyphicon-info-sign"></span></sup></button>
				<?php }
				elseif ($dimmfaehigkeit_beschreibung == 'Ja, 30 - 100%! Produkt nur in Verbindung mit LED kompatiblen Dimmern verwenden.' or $dimmfaehigkeit_beschreibung == 'Ja, 5 - 100%! Produkt nur in Verbindung mit LED kompatiblen Dimmern verwenden!'){?> 
				<button class="infolink" rel="clickover"  data-placement="top" data-toggle="popover" data-content="Die meisten handelsüblichen Dimmer funktionieren mit unseren Lampen - allerdings kann es vorkommen, dass Sie nicht vollständig Stufenlos dimmen können. - für besondere Anwendungen wie zB.: Theater oder Veranstaltungsort empfehlen wir den Einsatz von soft-LED Systemdimmern." title="Dimmfähigkeit<a class='close'>&times;</a>">

				<?php echo $dimmfaehigkeit_beschreibung ?>
				<sup><span class="glyphicon glyphicon-info-sign"></span></sup></button>


				<?php }
				else {
				echo $dimmfaehigkeit_beschreibung;
				} ?>
				</button></td>
			</tr>
					<tr class="">
				<th>Lichtfarbe / Kelvin</th>
				<td class="product_weight"><?php $lichtfarbe = get_field( "lichtfarbe-beschreibung" ); echo $lichtfarbe ?> / <?php $lichtfarbekelvin = get_field( "lichtfarbe" ); echo $lichtfarbekelvin ?>K</td>
			</tr>
		
					<tr class="alt">
				<th>Farbwiedergabe</th>
				<td class="product_dimensions"><button class="infolink" rel="clickover"  data-placement="right" data-toggle="popover" data-content="Der Farbwiedergabewert bezieht sich auf die Fähigkeit der LED Lampe, ein möglichst naturgetreues, von der Halogenlampe gewohntes Licht ohne jeglichen Farbstich zu erzeugen.</p><p>Handelsübliche LED-Lampen haben meistens einen Wert von weniger als 80 RA, was zu unangenehmen bläulichen / grünlichen Farbstich führen kann!
" title="Farbwiedergabe<a class='close'>&times;</a>"><?php $farbwiedergabe = get_field( "farbwiedergabewert" ); echo $farbwiedergabe ?><sup><span class="glyphicon glyphicon-info-sign"></span></sup></button></td>
			</tr>
					<tr class="">
				<th>Garantie</th>
				<td class="product_weight"><?php $garantie = get_field( "garantie_(monate)" ); echo $garantie ?> Monate</td>
			</tr>
		
					<tr class="alt">
				<th>Energieeffizienzklasse</th>
				<td class="product_dimensions"><?php $effizienz = get_field( "energielabel" ); if ($effizienz == "A1") { ?>A+<?php } if ($effizienz == "A2") {?>A++<?php } if ($effizienz == "A3") {?>A+++<?php } if ($effizienz == "A") {?>A<?php } ?></td>
			</tr>
					<tr class="">
				<th>Schaltzyklen</th>
				<td class="product_dimensions"><?php $schaltzyklen = get_field ( "schaltzyklen" ); echo number_format($schaltzyklen, 0, ',', '.'); ?> Schaltzyklen</td>
			</tr>
					<tr class="alt">
				<th>Zertifizierungen</th>
				<td class="product_weight">CE &amp; RoHS</td>
			</tr>
		<?php $dimensions =  $product->get_dimensions(); if ($dimensions ) { ?>
					<tr class="">

				<th>Maße inkl. Verpackung</th>
				<td class="product_dimensions"><?php echo $product->get_dimensions(); ?></td>
			</tr>
			<?php }
			$weight = $product->get_weight(); if ($weight) {  ?>
			<tr class="alt">
				<th>Gewicht Lampe</th>
				<td class="product_weight"><?php echo $product->get_weight(); ?> Gramm</td>
			</tr>
			<?php } ?>
						<?php $type = get_field( "type"); if ($type) { ?>
			<tr class="">
				<th>Typ:</th>
				<td class="product_weight"><?php echo $type; ?></td>
			</tr><?php } else echo ''; ?>
	</tbody></table>
	</div>
</div>
</div>
<div class="col-md-5">
<?php
$kerzenlicht = get_field("kerzenlicht");
$filament = get_field("filament");
$form = get_field ("form");
$lichtfarbe_desc = get_field("lichtfarbe-beschreibung");
if ($kerzenlicht == TRUE) {?>
<div class="atmo_image_container_black">
<img class="atmo_image" src="http://www.ledprofi.com/wp-content/uploads/2015/06/candle-light-dinner.jpg">
</div>
<?php }
if ($filament == TRUE && $form != "Globe" && $form != "Kerzenlampe" && lichtfarbe_desc != "naturweiß" && $kerzenlicht == FALSE) {?>
<div class="atmo_image_container_black">
<img class="atmo_image" src="http://www.ledprofi.com/wp-content/uploads/2015/06/glühbirne_filament.jpg">
</div>
<?php }

if ($form == "Spot" && $lichtfarbe_desc == "warmweiß" or $form == "Spot" && $lichtfarbe_desc == "extrawarmweiß" or $form == "Spot" && $lichtfarbe_desc == "extra warmweiß" or $form == "Spot" && $lichtfarbe_desc == "supersoft warmweiß") {?>
<div class="atmo_image_container">
<img class="atmo_image" src="http://www.ledprofi.com/wp-content/uploads/2015/06/spot_warmweiß.png">
</div>
<?php }
if ($filament == FALSE && $lichtfarbe_desc == "naturweiß") {?>
<div class="atmo_image_container">
<img class="atmo_image" src="http://www.ledprofi.com/wp-content/uploads/2015/06/neutralweiß_banner.png">
</div>
<?php }
if ($form == "Kerzenlampe" && $filament == TRUE && $kerzenlicht == FALSE) {?>
<div class="atmo_image_container">
<img class="atmo_image" src="http://www.ledprofi.com/wp-content/uploads/2015/06/kerzenlampe_filament.jpg">
</div>
<?php }
elseif ($form != "Spot" && $lichtfarbe_desc == "warmweiß" && $filament == FALSE or $form == "Globe") {?>
<div class="atmo_image_container">
<img class="atmo_image" src="http://www.ledprofi.com/wp-content/uploads/2015/06/warmweiß_else.jpeg">
</div>
<?php }
?>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div id="details" class="panel panel-default">
	<div class="panel-heading">
		<h2 class="panel-title">Details</h2>
	</div>
	<div class="panel-body">
	<div class="row">
<div class="col-md-6">
<h4>Kann man mit LED Lampen auch angenehmes, warmes Licht erzeugen?</h4>
<p>Die Kennzahl für die Farbtemperatur ist Kelvin (K). Eine unserem Modell ähnliche Glühbirne oder Halogenbirne hat üblicherweise <?php echo $lichtfarbekelvin ?>K.</p>
<p><?php $prefix_big = get_field("prefix_bezeichnung_gross"); echo $prefix_big; ?> LED-<?php echo $form; ?> hat, im Gegensatz zu vielen Chinaimitaten und selbst Markenprodukten, echte <?php echo $lichtfarbekelvin ?>K und erzeugt daher ein einzigartig farbgetreues Licht, bei dem Sie keinen Unterschied zu ihrer Halogen- oder Glühbirne bemerken werden - außer bei der Energieabrechnung am Ende des Jahres!</p>



</div>
<div class="col-md-6">
<h4>Qualitätsunterschiede bei LED Lampen, selbst bei Markenherstellern</h4>
<p>Machen Sie keine Kompromisse bei der Lichtqualität, herkömmliche LED Lampen mangelt es oft an genauer Farbwiedergabe, was zu einem unangenehmen, weißlich oder bläulichen Licht führt.</p>

<p>Weiters haben viele LED Lampen (auch welche mit Glühfadentechnik), auf Grund von billig verbauten Komponenten, Probleme mit ortsüblichen Spannungsschwankungen - was zu einem unangenehmen Flimmern führt.</p>
<p>LEDprofi.com bietet ausschließlich LED Lampen in Spitzenqualität, die in Österreich konstruiert &amp; getestet werden. Die Produktion findet bei unserem Partner in China unter strenger Qualitätskontrolle statt.</p>


</div>
</div>
<hr style="margin-top:20px" />
<div class="row">
<div class="col-md-6">

<h4>Was bedeutet LED?</h4>
<p>LED steht für „Light Emitting Diode“, also spezielle Halbleiterverbindungen, die durch Strom zum Leuchten angeregt werden. Hierbei handelt es sich um die modernste Art und Weise Licht zu erzeugen. Dies birgt eine Reihe von besonderen Vorteilen.</p>
<ul class="infolist">
<li>Geringe Energiekosten (ca. 80-90% gespart, da LEDs viel effizienter Energie in Licht umwandeln können)</li>
<li>Langlebigkeit - <?php $prefix_small = get_field("prefix_bezeichnung_klein"); echo $prefix_small; ?> <?php echo $form; ?> hält mindestens <?php echo number_format((float)$lifetime, 0, ',', ''); ?> Jahre bei 4 Stunden Verwendung pro Tag</li>
<li>ÖkoLOGISCH - LEDs sind vollkommen ungiftig, enthalten kein Quecksilber oder sonstige giftige Stoffe und der Großteil der Bauteile kann als wertvoller Elektroschrott wiederverwertet werden</li>
</ul>
</div>
<div class="col-md-6">
<h4>Wieso ist unsere LED-<?php echo $form; ?> besser als bisherige Lampen?</h4>

<p><?php $prefix_big = get_field("prefix_bezeichnung_gross"); echo $prefix_big; ?> <?php echo $form; ?> ist im neuen Jahrtausend angekommen.</p>

<p>Hoher Energieverbrauch, kurze Lebensdauer und Abstrahlhitze sind dabei Geschichte, ohne auf die Vorteile der Glühdraht bzw. Halogentechnik verzichten zu müssen!</p><p>Perfekte <?php $lichtfarbe = get_field( "lichtfarbe-beschreibung" ); echo $lichtfarbe; ?>e Lichtfarbe<?php 
	$abstrahlwinkel = get_field ("abstrahlwinkel");
	if ($abstrahlwinkel == "360") {
		echo ", 360°-Rundstrahlverhalten";
	}
	else {
		echo '';
	}

 ?>
 <?php $halogenretrooptik = get_field("retro-look"); if ($halogenretrooptik) {echo ', sowie 100% ' . $halogenretrooptik; } ?> und 100%-ige Einsatzfähigkeit in vorhandenen Lampenfassungen bleiben erhalten.</p>
 <p>
 <?php $prefix_big = get_field("prefix_bezeichnung_gross"); echo $prefix_big; ?> <?php echo $form; ?> verbraucht dabei allerdings nur einen Bruchteil der Energie (-<?php
 $birnenleistung = get_field("birnenleistung");
 $watt = get_field("watt");
$bruchteil1 = $watt / $birnenleistung;
$bruchteil2 = 1 - $bruchteil1;
$bruchteil3 = $bruchteil2 * 100;
echo number_format((float)$bruchteil3, 2, ',', '');;
  ?>%). Sie sparen dabei ca. <button class="infolink" rel="clickover"  data-placement="top" data-toggle="popover" data-content="Die Angaben basieren auf den durchschnittlichen Österreichischen Energiepreis brutto in Höhe von 20,2 Eurocent pro kWh - Stand 2015.<br /> <br />Quelle: http://oesterreichsenergie.at/daten-fakten/statistik/Strompreis.html"><?php $energiekosten = 0.19 ; $lebensdauer_in_stunden = get_field ("lebensdauer_in_stunden"); $kwh_1000h = get_field( "leistungsaufnahme_watt_genau" ); $kwh_2 = $kwh_1000h / 1000; $preis_led = $lebensdauer_in_stunden * $kwh_2 * $energiekosten; 
$birnenleistung = get_field("birnenleistung");
$preis_vergleich = $birnenleistung * $lebensdauer_in_stunden / 1000 * $energiekosten ; 

$preis_ende = $preis_vergleich - $preis_led; echo number_format((float)$preis_ende, 2, ',', '');  ?> €<sup><span class="glyphicon glyphicon-info-sign"></span></sup></button> an Energiekosten über die Lebensdauer gerechnet.
 </p>
	</div>
	</div>
<?php
$lichtfarbe_desc = get_field("lichtfarbe-beschreibung");
 if ($lichtfarbe_desc == "warmweiß") {
	?>
<hr style="margin-top:20px" />
<h4 class="ledprofigarantie">Farbtemperatur dieser LED</h4>
<div class="row">
<div class="col-md-3">
</div>
<div class="col-md-2" style="text-align:center;">
<span style="font-size:12px;">für stimmungsvolles Licht</span>
<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/extrawarmweiß.jpg" class="showcasetemp not" />
<p style="text-align:center;">&lt;2200K<br/>extrawarmweiß</p>

</div>
<div class="col-md-2" style="text-align:center;">
<span style="font-size:12px;">wie eine echte Glühbirne</span>
<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/warmweiß.jpg" class="showcasetemp" />
<p style="text-align:center;font-weight:bold;">2700K-3300K<br/>warmweiß</p>
<div class="triangle"></div>

</div>
<div class="col-md-2" style="text-align:center;">
<span style="font-size:12px;">ideal für Büro &amp; Bad</span>
<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/kaltweiß.jpg" class="showcasetemp not" />
<p style="text-align:center;">&gt;3300K<br/>naturweiß</p>
</div>
<div class="col-md-3">
</div>
</div>
<?php  }?>
<?php if ($lichtfarbe_desc == "naturweiß") {
	?>
<hr style="margin-top:20px" />
<h4 class="ledprofigarantie">Farbtemperatur dieser LED</h4>

<div class="row">
<div class="col-md-3">
</div>
<div class="col-md-2" style="text-align:center;">
<span style="font-size:12px;">für stimmungsvolles Licht</span>
<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/extrawarmweiß.jpg" class="showcasetemp not" />
<p style="text-align:center;">&lt;2200K<br/>extrawarmweiß</p>

</div>
<div class="col-md-2" style="text-align:center;">
<span style="font-size:12px;">wie eine echte Glühbirne</span>
<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/warmweiß.jpg" class="showcasetemp not" />
<p style="text-align:center;">2700K-3300K<br/>warmweiß</p>
</div>
<div class="col-md-2" style="text-align:center;">
<span style="font-size:12px;">ideal für Büro &amp; Bad</span>
<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/kaltweiß.jpg" class="showcasetemp" />
<p style="text-align:center;font-weight:bold;">&gt;3300K<br/>naturweiß</p>
<div class="triangle"></div>
</div><div class="col-md-3">
</div>
</div>
<?php  }?>
<?php if ($lichtfarbe_desc == "supersoft warmweiß" or $lichtfarbe_desc == "extra warmweiß" or $lichtfarbe_desc == "extrawarmweiß"  ) {
	?>
<hr style="margin-top:20px" />
<h4 class="ledprofigarantie">Farbtemperatur dieser LED</h4>

<div class="row">

<div class="col-md-3">
</div>
<div class="col-md-2" style="text-align:center;">
<span style="font-size:12px;">für stimmungsvolles Licht</span>
<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/extrawarmweiß.jpg" class="showcasetemp" />
<p style="text-align:center;font-weight:bold;">&lt;2200K<br/>extrawarmweiß</p>
<div class="triangle"></div>
</div>
<div class="col-md-2" style="text-align:center;">
<span style="font-size:12px;">wie eine echte Glühbirne</span>
<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/warmweiß.jpg" class="showcasetemp not" />
<p style="text-align:center;">2700K-3300K<br/>warmweiß</p>
</div>
<div class="col-md-2" style="text-align:center;">
<span style="font-size:12px;">ideal für Büro &amp; Bad</span>
<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/kaltweiß.jpg" class="showcasetemp not" />
<p style="text-align:center;">&gt;3300K<br/>naturweiß</p>
</div><div class="col-md-3">
</div>
</div>
<?php  }?>
<hr style="margin-top:20px" />
<h4 class="ledprofigarantie">Die LEDprofi.com-Garantie</h4>
<div class="row">
<div class="ledprofi_garantie_icons col-md-4">
<span class="glyphicon glyphicon-refresh"></span>
<h5>30-Tage Widerrufsrecht</h5>
</div>
<div class="ledprofi_garantie_icons  col-md-4">
<i class="fa fa-check"></i>
<h5><?php $garantie = get_field( "garantie_(monate)" )/12; echo $garantie ?> Jahre Garantie</h5>
</div>
<div class="ledprofi_garantie_icons  col-md-4">
<i class="fa fa-leaf"></i>
<h5>Nachhaltig durch Effizienz und Qualität</h5>
</div>
</div>
<div class="row">
<div class="col-md-6">
<ul class="infolist">
<li>Unsere LED-Lampen liefern garantiert <?php $lichtfarbe = get_field( "lichtfarbe-beschreibung" ); echo $lichtfarbe; ?>es Licht, wie bei <?php
$form = get_field("form");
if ($form == "Glühbirne" or $form == "Kerzenlampe" or $form == "Mini ILLU Birne" or $form == "Globe") {
	echo "einer herkömmlichen"; };
if ($form == "G9 Leuchtmittel" or $form == "Spot") {
	echo "einem herkömmlichen";
}
?>
 <?php echo $form; ?> und können 1-zu-1 in die vorhandene Fassung eingesetzt werden.
 </li>


</div>
<div class="col-md-6">
<ul class="infolist">
<?php $energiekosten = 0.19; ?>
<li>Sie sparen mindestens 80% der Energiekosten (von den zusätzlichen Anschaffungskosten für Ersatzbirnen ganz zu schweigen), was sich allein bei diesem Modell pro Stück auf insgesamt <button class="infolink" rel="clickover"  data-placement="top" data-toggle="popover" data-content="Die Angaben basieren auf den durchschnittlichen Österreichischen Energiepreis brutto in Höhe von 20,2 Eurocent pro kWh - Stand 2015.<br /> <br />Quelle: http://oesterreichsenergie.at/daten-fakten/statistik/Strompreis.html"><?php $energiekosten = 0.19 ; $lebensdauer_in_stunden = get_field ("lebensdauer_in_stunden"); $kwh_1000h = get_field( "leistungsaufnahme_watt_genau" ); $kwh_2 = $kwh_1000h / 1000; $preis_led = $lebensdauer_in_stunden * $kwh_2 * $energiekosten; 
$birnenleistung = get_field("birnenleistung");
$preis_vergleich = $birnenleistung * $lebensdauer_in_stunden / 1000 * $energiekosten ; 

$preis_ende = $preis_vergleich - $preis_led; echo number_format((float)$preis_ende, 2, ',', '');  ?> €<sup><span class="glyphicon glyphicon-info-sign"></span></sup></button> über die gesamte Lebensdauer summiert!</li>
</ul>
</div>
</div>

<div class="row">
<div class="col-md-6">
<ul class="infolist">
<li>Unsere LED-Lampen werden in Opern, Theatern, dem österreichischen Parlament und der Österreichischen Nationalbibliothek, sowie in öffentlichen, wie privaten Häusern eingesetzt, weil sie bei zahlreichen Tests als absoluter Testsieger in Punkto Lichtqualität &amp; Lebensdauer hervorgegangen sind!</li>

</div>
<div class="col-md-6">
<ul class="infolist">
<li>Unsere LED-Lampen schalten sich sofort ein und haben eine unglaubliche Lebensdauer von bis zu 50.0000 Betriebsstunden (das entspricht etwa der durchschnittlichen Lebensdauer von 25 bis 50 Glüh- oder Halogenbirnen (eine durchschnittliche Halogenbirne hat eine maximale Lebensdauer zwischen 1.000-2.000 Stunden)!</li>
</ul>
</div>
</div>
<hr>
<div class="row">
<div class="col-md-3">
<h4>Haben Sie noch Fragen?</h4>
<p>Gerne hilft Ihnen das LEDprofi.com-Team bei der richtigen Wahl Ihrer Beleuchtungsmittel.</p><p>Schreiben Sie uns einfach eine <a target="_blank" href="http://www.ledprofi.com/kontakt/"><span class="glyphicon glyphicon-envelope"></span> Mail</a> oder rufen Sie uns zu unseren Geschäftszeiten unter der Telefonnummer <a href="tel:0043720884134"><span class="glyphicon glyphicon-phone-alt"></span> +43 (0) 720 884134</a> an.
</p>
</div>
<div class="col-md-3">
<h4>Zusammenfassung</h4>
<ul class="above_the_fold_info"><?php global $product; if( $product->is_type( 'bundle' ) ){?>
	<li><strong><?php $stueckzahl = get_field("stueckzahl"); echo $stueckzahl; ?>er-Pack</strong></li><?php }?>
	<li><?php $bezeichnung = get_field( "bezeichnung_1" ); echo $bezeichnung ?> <?php $watt = get_field( "watt" ); echo $watt ?>W</li>
	<?php $dimmbar = get_field( "dimmbar" )?>
	<?php if ($dimmbar) { ?><li>dimmbar</li> <?php } else echo ''; ?>
	<li>ersetzt <?php $birnenleistung = get_field( "birnenleistung" )?><?php  echo $birnenleistung?> Watt</li><li>Sockel <?php $fassung = get_field( "fassung" )?><?php  echo $fassung?> - <?php $lichtfarbe = get_field( "lichtfarbe-beschreibung" )?><?php  echo $lichtfarbe?> (<?php $lichtfarbekelvin = get_field( "lichtfarbe" )?><?php  echo $lichtfarbekelvin?>K)</li>
	<li>hält <?php $lebensdauer_in_stunden = get_field( "lebensdauer_in_stunden" ); echo number_format($lebensdauer_in_stunden, 0, ',', '.'); ?> Stunden</li>
<li><?php $garantie = get_field( "garantie_(monate)" )/12; echo $garantie ?> Jahre Garantie</li>
<li>Energieeffizienzklasse: <?php $effizienz = get_field( "energielabel" ); if ($effizienz == "A1") { ?>A+<?php } if ($effizienz == "A2") {?>A++<?php } if ($effizienz == "A3") {?>A+++<?php } if ($effizienz == "A") {?>A<?php } ?></li></ul>
	</div>
<div class="col-md-3">
<?php $image_link  	= wp_get_attachment_url( get_post_thumbnail_id() );?>
<img class="lastcall" src="<?php echo $image_link?>" />
</div>
<div class="col-md-3">
<h4>Überzeugt?</h4>
<p>Dann bestellen Sie jetzt!</p>

				<?php
					/**
					 * woocommerce_single_product_summary hook
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 */
					do_action( 'woocommerce_single_product_to_cart' );
				?>
</div>

</div>
</div>
</div>
</div>
</div>

<?php
}

function woocommerce_in_stock() {?>



<?php
global $product;
if( $product->is_type( 'simple' ) ) {
 $coming_soon_1 = get_field("coming_soon_1"); $coming_soon_2 = get_field("coming_soon_2"); $verfugbar = get_field("verfugbar"); 
 if ($verfugbar) { ?>

<span class="stock">Auf Lager</span>
<?php }
if ($coming_soon_1) {?>
<span class="stock">
In Produktion
</span>
<?php }
if ($coming_soon_2) {?>
<span class="stock">
In Produktion</span>
<?php }
if ($coming_soon_2 == FALSE && $coming_soon_1 == FALSE && $verfugbar == FALSE) {?>
<span class="stock" style="color: #c8655c !important;">Nicht verfügbar</span>
<?php } ?>
 <p class="express-lieferung"><strong>
 <?php $coming_soon_1 = get_field("coming_soon_1");$coming_soon_2 = get_field("coming_soon_2");$verfugbar = get_field("verfugbar"); if ($verfugbar) { ?>Lieferung innerhalb von 1-2 Werktagen.
 <?php }
if ($coming_soon_1) {?>Lieferung innerhalb von 1-2 Wochen
<?php }
if ($coming_soon_2) {?>
Lieferung innerhalb von 6-8 Wochen
<?php }
if ($coming_soon_2 == FALSE && $coming_soon_1 == FALSE && $verfugbar == FALSE) {?>
Leider ist dieses Produkt nicht erhältlich.
<?php } ?>
 </strong><br />Versand per Post bzw. DPD. <span class="versandkostenfrei_2">Versandkostenfrei.</span><span class="zzgl_2">Ab <?php global $versandkostengrenze; echo $versandkostengrenze; ?> Warenwert Versand gratis innerhalb Österreichs.</span></p> <?php } }

function woocommerce_details_above_fold() {?>
<?php
	$form = get_field( "form" );
	$birnenleistung = get_field( "birnenleistung" );
	$lumen = get_field( "lumen_l70" );

if( $form != "Spot" && $form != "Globe" ) {?><p class="ersatz_info">
		Die <strong>Glühbirne</strong> ist zurück - nur besser! So hell wie <strong><?php echo $birnenleistung ?> Watt</strong> - <?php echo $lumen ?> Lumen.
	</p>
	<?php
} if ($form == "Spot") {?>
<p class="ersatz_info">
    <strong>1-zu-1</strong> Halogen Spot Ersatz - nur besser! So hell wie <strong><?php echo $birnenleistung ?> Watt</strong> - <?php echo $lumen ?> Lumen.</p><?php

}
if ($form == "Globe") {?>
<p class="ersatz_info">
    <strong>Riesenbirne</strong> im Retrodesign - so hell wie <strong><?php echo $birnenleistung ?> Watt</strong> - <?php echo $lumen ?> Lumen.</p>
	<?php }?>
<ul class="above_the_fold_details">
	<?php $lichtfarbekelvin = get_field( "lichtfarbe" );
			if ($lichtfarbekelvin <= 2200 && $form != "Spot") { print("<li>Warmes <strong>Kerzenlicht wie bei einer vergleichbaren Glühbirne</strong>"); }
			if ($lichtfarbekelvin >= 4000 && $form != "Spot") {
				print("<li>Echtes naturweißes <strong>Licht wie bei einer vergleichbaren Glühbirne</strong>"); }  
			if ($lichtfarbekelvin > 2200 && $lichtfarbekelvin <= 3000 && $form != "Spot" && $form !="G9 Leuchtmittel") { 
				print("<li>Echtes warmweißes <strong>Licht wie bei einer vergleichbaren Glühbirne</strong>"); } 
			if ($lichtfarbekelvin > 2200 && $lichtfarbekelvin <= 3000 && $form =="G9 Leuchtmittel") { 
				print("<li>Echtes warmweißes <strong>Licht wie bei einer vergleichbaren Lampe</strong>"); } 
			if ($lichtfarbekelvin <= 2200 && $form == "Spot") { print("<li>Warmes <strong>Kerzenlicht wie bei einem vergleichbaren Halogen-Spot</strong>"); }
			if ($lichtfarbekelvin >= 4000 && $form == "Spot") {
				print("<li>Echtes naturweißes <strong>Licht wie bei einem vergleichbaren Halogen-Spot</strong>"); }  
			if ($lichtfarbekelvin > 2200 && $lichtfarbekelvin <= 3000 && $form == "Spot") { 
				print("<li>Echtes warmweißes <strong>Licht wie bei einem vergleichbaren Halogen-Spot</strong>"); } 

			?> 

		(<?php echo $lichtfarbekelvin ?>K)</li>
	<?php
	$premium = get_field( "premium" );
	if( $premium ) {
	?><li><button id="premium" class="infolink" rel="clickover"  data-placement="top" data-toggle="popover" data-content="
<p>Für Personen, die keine Kompromisse im Vergleich zu Halogenlampen machen möchten. In soft-LED Premium Produkten werden nur die besten Chipbausteine &amp; Materialien verbaut!</p><p><strong>Das Ergebnis:</strong></p><ul class='above_the_fold_details'><li>Perfekte Farbwiedergabe über die gesamte Lebensdauer (<?php $farbwiedergabewert = get_field("farbwiedergabewert"); echo $farbwiedergabewert; ?>)</li><li>Überragende Verarbeitungsqualität sowie Retrodesign, dass nicht von herkömmlichen Glühbirnen / Spots unterschieden werden kann</li><li>Die selbe Technologie, die 2014 im Rahmen des Topprodukte Österreich Tests als absolut überlegen gegenüber vergleichbaren LED-Lampen abgeschnitten hat</li></ul>" title="Premium<a class='close'>&times;</a>">
		<strong>Premium-LED:</strong> <?php $garantie = get_field( "garantie_(monate)" )/12; echo $garantie ?> Jahre Garantie - <strong>beste Farbwiedergabe <?php $farbwiedergabewert = get_field( "farbwiedergabewert" ); echo $farbwiedergabewert ?><sup><span class="glyphicon glyphicon-info-sign"></span></sup></button></strong>
	</li>
		<?php
} else {?>

    	<li>
		Kein Farbstich, <strong>100% Retro-Fit</strong>
	</li>
    
<?php } ?>
	<li><button style="padding-left:0px!important;padding-right:0px!important;" id="lebensdauer" class="infolink" data-placement="top" data-toggle="popover" data-content="
<ul class='above_the_fold_details'><li>Betriebsdauer dieser LED: <?php $lebensdauer_in_stunden = get_field( "lebensdauer_in_stunden" ); echo number_format($lebensdauer_in_stunden, 0, ',', '.'); ?> Stunden</li>
<li>bei 4 Stunden pro Tag ergibt das <?php $lifetime = get_field( "lebensdauer_in_jahren_4h_pro_tag" ); echo number_format((float)$lifetime, 0, ',', '');  ?> Jahre</li></ul>
<br />
<p><strong>Zum Vergleich:</strong></p>
<ul class='above_the_fold_details'><li>Halogenbirne: 2000 Stunden (sie bräuchten dafür <?php $halogen = $lebensdauer_in_stunden / 2000; echo $halogen; ?> Lampen für ca. <?php $halogen_kosten = $halogen*3; echo number_format((float)$halogen_kosten, 2, ',', ''); ?> €) </li>
<li>Energiesparlampe: 4000 Stunden (sie bräuchten dafür <?php $sparlampe = $lebensdauer_in_stunden / 4000; echo number_format((float)$sparlampe, 0, ',', '');; ?> Lampen für ca. <?php $sparlampe_kosten = $sparlampe*3; echo number_format((float)$sparlampe_kosten, 2, ',', ''); ?> €)</li>
<li>Glühbirne: 1000 Stunden (sie bräuchten dafür <?php $birne = $lebensdauer_in_stunden / 1000; echo $birne; ?> Glühbirnen)</li>" title="Lebensdauer<a class='close'>&times;</a>">
			<?php $lifetime = get_field( "lebensdauer_in_jahren_4h_pro_tag" ); if ($lifetime >=20) { ?>
			<strong>"Nie mehr" Lampe wechseln:</strong> <strong>&gt;<?php $lifetime = get_field( "lebensdauer_in_jahren_4h_pro_tag" ); echo number_format((float)$lifetime, 0, ',', ''); ?> Jahre</strong> Lebensdauer,
			 <?php } else {?>
			<strong>Besonders hohe Lebensdauer:</strong> mehr als
			<strong><?php $lifetime = get_field( "lebensdauer_in_jahren_4h_pro_tag" ); echo number_format((float)$lifetime, 0, ',', ''); ?> Jahre</strong>,
			<?php } $schaltzyklen = get_field( "schaltzyklen" );?>

	<?php if ($schaltzyklen == 1000000) {?>1 Million <?php } else {echo number_format($schaltzyklen, 0, ',', '.'); }?> Schaltzyklen<sup><span class="glyphicon glyphicon-info-sign"></span></sup>
		</button>

	</li>
 	<li>
		Designed in <img class="austria-flag" alt="Austria" src="http://www.ledprofi.com/wp-content/uploads/2015/05/Austria.png"> <span class="eu_label">EU</span>
	</li>
 </ul><?php
}


function woocommerce_bundle() { 



    global $product;
    $upsell1 = get_field( "upsell-paket-1" );
    $upsell2 = get_field( "upsell-paket-2" );
if( $product->is_type( 'simple' ) && $upsell1 != '' && $upsell2 != '' ){ ?>
 <button class="infolink" rel="clickover"  data-placement="top" data-toggle="popover" data-content="Unsere Packs werden in Großpackungen geliefert. Daher können wir bessere Preise als bei der Bestellung von Einzelverpackungen anbieten." title="Vorratspackungen<a class='close'>&times;</a>"><h5 class="vorrat">Vorratspackungen<sup><span class="glyphicon glyphicon-info-sign"></span></sup></button></h5>
 <div class="bundle">
 
<?php $coming_soon_1 = get_field("coming_soon_1"); $coming_soon_2 = get_field("coming_soon_2"); $verfugbar = get_field("verfugbar"); if ($verfugbar or $coming_soon_2 or $coming_soon_1) {?>

 <a href="<?php $baseurl = get_bloginfo('wpurl'); echo $baseurl; ?>/produkt/<?php $upsell1 = get_field( "upsell-paket-1" ); echo $upsell1 ?>er-pack-<?php echo get_query_var('name'); ?>/">
 <div class="first_bundle">
  <div class="bundle-left">
    <span class="glyphicon glyphicon-piggy-bank"></span>
  </div>
  <div class="bundle-right">
 <span class="bundlepack"><?php $upsell1 = get_field( "upsell-paket-1" ); echo $upsell1 ?>er-Pack</span>
 <span class="bundleprice"><?php 

$base_sku_pre = $product->get_sku();
$base_sku = substr($base_sku_pre, 0, -3);
$upsell1_sku = $base_sku .  '-0' . $upsell1;
$upsell1_id = wc_get_product_id_by_sku ( $upsell1_sku );
$upsell1_product = new WC_Product( $upsell1_id );
$upsell1_price = $upsell1_product->get_price_html();

echo $upsell1_price;


 ?> (-<?php $uvp = get_field("uvp"); $productxfirst = $uvp * $upsell1; $upsell1_price_simple = $upsell1_product->get_price() *1.2; $packagesavings1 = $productxfirst - $upsell1_price_simple ; $packagesavings1percentage = $packagesavings1 / $productxfirst; echo number_format((float)$packagesavings1percentage*100, 0, ',', ''); ?>%)</span>
 </div>
 </div>
 </a>
 </div>
  <div class="bundle">
  <a href="<?php $baseurl = get_bloginfo('wpurl'); echo $baseurl; ?>/produkt/<?php $upsell2 = get_field( "upsell-paket-2" ); echo $upsell2 ?>er-pack-<?php echo get_query_var('name'); ?>/">
  <div class="second_bundle">
  <div class="bundle-left">
    <span class="glyphicon glyphicon-piggy-bank"></span>
  </div>
  <div class="bundle-right">
 <span class="bundlepack"><?php $upsell2 = get_field( "upsell-paket-2" ); echo $upsell2 ?>er-Pack</span>
 <span class="bundleprice"><?php 
if ($upsell2 >=10) {
$upsell2_sku = $base_sku .  '-' . $upsell2;
$upsell2_id = wc_get_product_id_by_sku ( $upsell2_sku );
$upsell2_product = new WC_Product( $upsell2_id );
$upsell2_price = $upsell2_product->get_price_html();

echo $upsell2_price;
}
else {
$upsell2_sku = $base_sku .  '-0' . $upsell2;
$upsell2_id = wc_get_product_id_by_sku ( $upsell2_sku );
$upsell2_product = new WC_Product( $upsell2_id );
$upsell2_price = $upsell2_product->get_price_html();

echo $upsell2_price;
}
 ?> (-<?php
$upsell2_net_price = $upsell2_product->price;
$uvp_upsell2 = $uvp_integr * $upsell2;
$uvp = get_field( "uvp" );
$uvp_integr = $int = ereg_replace("[^0-9]", "", $uvp);
$upsell2_integr = $int = ereg_replace("[^0-9]", "", $upsell2_net_price);


 ?><?php $uvp = get_field("uvp"); $productxsec = $uvp * $upsell2; $upsell2_price_simple = $upsell2_product->get_price() *1.2; $packagesavings2 = $productxsec - $upsell2_price_simple ; $packagesavings2percentage = $packagesavings2 / $productxsec; echo number_format((float)$packagesavings2percentage*100, 0, ',', ''); ?>%)</span>
 </div>
 </div>
  </a>
  <?php  } else {?><style>.five_plus_one {display:none!important;}</style><?php }?>
 </div>
<?php
} elseif( $product->is_type( 'bundle' ) ){?>
 <div class="bundle">

 <a href="<?php $singlepackageurl = get_field( "_yoast_wpseo_canonical" ); echo $singlepackageurl; ?>">
  <div class="bundle-left">
    <span style="font-size:20px;" class="glyphicon glyphicon-arrow-left"></span>
  </div>
  <div class="bundle-right">
 <span style="font-size:12px;" class="bundlepack">Zurück zum</span>
 <span class="bundleprice">Einzelprodukt</span>
 </div>
 </a>
 </div>
<?php
 }
 else {?>
 <style>.five_plus_one {display:none;}</style>
 <?php

 }
}
function woocommerce_garantie() { ?>
 <h5 class="vorrat">Garantie</h5>
 <div class="bundle">
 <span class="bundlepack">XYZ</span>
 <span class="bundleprice">asdf</span>
 </div>
  <div class="bundle">
 <span class="bundlepack">XYZ</span>
 <span class="bundleprice">asdf</span>
 </div><?php
 }
 