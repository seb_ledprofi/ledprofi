<?php
/**
 * Archive Template for the store
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0	
 */

?>

<?php
/**
 * File Security Check
 */
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js lt-ie9 lt-ie8 ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js lt-ie9 ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>

<meta charset="UTF-8">
<title>G9 LED Leuchtmittel / Lampen vom LEDprofi</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Jetzt die passende G9 LED Lampe finden! Perfektes Licht in Topqualität ☀ Spart Strom mit höchster Effizienz ✓">
<link rel="canonical" href="http://www.ledprofi.com/led-g9/">
<meta property="og:locale" content="de_DE">
<meta property="og:type" content="website">
<meta property="og:title" content="G9 LED Leuchtmittel / Lampen vom LEDprofi">
<meta property="og:description" content="Jetzt die passende G9 LED Lampe finden! Perfektes Licht in Topqualität ☀ Spart Strom mit höchster Effizienz ✓">
<meta property="og:url" content="http://www.ledprofi.com/led-g9/">
<meta property="og:site_name" content="LEDprofi.com">
<meta name="twitter:card" content="summary">
<meta name="twitter:description" content="Jetzt die passende G9 LED Lampe finden! Perfektes Licht in Topqualität ☀ Spart Strom mit höchster Effizienz ✓">
<meta name="twitter:title" content="G9 LED Leuchtmittel / Lampen vom LEDprofi">
<meta name="twitter:domain" content="LEDprofi.com">

<link rel="pingback" href="http://www.ledprofi.com/xmlrpc.php">
		<script type="text/javascript" charset="utf-8" src="//widgets.trustedshops.com/tbHelper.js"></script><script src="http://script.crazyegg.com/pages/scripts/0037/9326.js?399902" async="" type="text/javascript"></script><script type="text/javascript" charset="utf-8" async="" src="//widgets.trustedshops.com/js/XB95D06C5898B521EB4DCF9DB5B9C996D.js"></script><script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/www.ledprofi.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.2.4"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel="stylesheet" id="quote-request-style-css" href="http://cdn.ledprofi.com/wp-content/plugins/woocommerce-quotation/assets/css/style.css?ver=4.2.4" type="text/css" media="all">
<link rel="stylesheet" id="woocommerce-layout-css" href="//www.ledprofi.com/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=2.3.11" type="text/css" media="all">
<link rel="stylesheet" id="woocommerce-smallscreen-css" href="//www.ledprofi.com/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=2.3.11" type="text/css" media="only screen and (max-width: 768px)">
<link rel="stylesheet" id="woocommerce-general-css" href="//www.ledprofi.com/wp-content/plugins/woocommerce/assets/css/woocommerce.css?ver=2.3.11" type="text/css" media="all">
<link rel="stylesheet" id="woocommerce-gzd-layout-css" href="//www.ledprofi.com/wp-content/plugins/woocommerce-germanized/assets/css/woocommerce-gzd-layout.min.css?ver=1.3.4" type="text/css" media="all">
<style id="woocommerce-gzd-layout-inline-css" type="text/css">
.woocommerce-checkout .shop_table { background-color: #eeeeee; }
</style>
<link rel="stylesheet" id="dashicons-css" href="http://cdn.ledprofi.com/wp-includes/css/dashicons.min.css?ver=4.2.4" type="text/css" media="all">
<link rel="stylesheet" id="thickbox-css" href="http://cdn.ledprofi.com/wp-includes/js/thickbox/thickbox.css?ver=4.2.4" type="text/css" media="all">
<link rel="stylesheet" id="search-filter-chosen-styles-css" href="http://cdn.ledprofi.com/wp-content/plugins/search-filter-pro/public/assets/css/chosen.min.css?ver=1.4.3" type="text/css" media="all">

<link rel="stylesheet" id="search-filter-plugin-styles-css" href="http://cdn.ledprofi.com/wp-content/plugins/search-filter-pro/public/assets/css/search-filter.min.css?ver=1.4.3" type="text/css" media="all">
<link rel="stylesheet" id="opensans-css" href="//fonts.googleapis.com/css?family=Open+Sans:600,600italic,400,300,400italic,700,700italic,300italic" type="text/css" media="all">
<link rel="stylesheet" id="nudie-css" href="http://cdn.ledprofi.com/wp-content/themes/nudie/style.css?ver=2.1.3" type="text/css" media="all">
<link rel="stylesheet" id="child-theme-style-css" href="http://cdn.ledprofi.com/wp-content/themes/LEDprofi/style.css?ver=1.0" type="text/css" media="all">
<link rel="stylesheet" id="wc-bundle-style-css" href="http://cdn.ledprofi.com/wp-content/plugins/woocommerce-product-bundles/assets/css/bundles-style.css?ver=4.9.3" type="text/css" media="all">
<!-- This site uses the Google Analytics by Yoast plugin v5.4.2 - https://yoast.com/wordpress/plugins/google-analytics/ -->

<script type="text/javascript" async="" src="http://www.google-analytics.com/plugins/ua/linkid.js"></script>

<script async="" src="//www.google-analytics.com/analytics.js"></script>

<script type="text/javascript">
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

	__gaTracker('create', 'UA-62371051-1', 'auto');
	__gaTracker('set', 'forceSSL', true);
	__gaTracker('require', 'displayfeatures');
	__gaTracker('require', 'linkid', 'linkid.js');
	__gaTracker('send','pageview');

</script>
<!-- / Google Analytics by Yoast -->
<script type="text/javascript" src="http://cdn.ledprofi.com/wp-includes/js/jquery/jquery.js?ver=1.11.2"></script>
<script type="text/javascript" src="http://cdn.ledprofi.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1"></script>
<script type="text/javascript" src="http://cdn.ledprofi.com/wp-content/plugins/woocommerce-image-zoom/assets/js/jquery.elevateZoom-3.0.8.min.js?ver=3.0.8"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wc_additional_variation_images_local = {"ajaxurl":"http:\/\/www.ledprofi.com\/wp-admin\/admin-ajax.php","ajaxImageSwapNonce":"f497918566","gallery_images_class":".product .images .thumbnails","main_images_class":".product .images > a","lightbox_images":".product .images a.zoom","custom_swap":"","custom_original_swap":"","custom_reset_swap":""};
/* ]]> */
</script>
<script type="text/javascript" src="http://cdn.ledprofi.com/wp-content/plugins/woocommerce-additional-variation-images/assets/js/frontend.min.js?ver=4.2.4"></script>
<script type="text/javascript" src="http://cdn.ledprofi.com/wp-content/themes/nudie/js/modernizr.min.js?ver=2.8.3"></script>
<link rel="apple-touch-icon" sizes="57x57" href="/wp-content/uploads/fbrfg/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/wp-content/uploads/fbrfg/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/wp-content/uploads/fbrfg/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/wp-content/uploads/fbrfg/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/wp-content/uploads/fbrfg/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/wp-content/uploads/fbrfg/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/wp-content/uploads/fbrfg/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/wp-content/uploads/fbrfg/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/wp-content/uploads/fbrfg/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/wp-content/uploads/fbrfg/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/wp-content/uploads/fbrfg/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/wp-content/uploads/fbrfg/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/wp-content/uploads/fbrfg/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/wp-content/uploads/fbrfg/manifest.json">
<link rel="shortcut icon" href="/wp-content/uploads/fbrfg/favicon.ico">
<meta name="apple-mobile-web-app-title" content="LEDprofi.com">
<meta name="application-name" content="LEDprofi.com">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/wp-content/uploads/fbrfg/mstile-144x144.png">
<meta name="msapplication-config" content="/wp-content/uploads/fbrfg/browserconfig.xml">
<meta name="theme-color" content="#ffffff">  <style type="text/css">
    .screen-reader-response {
      display: none;
    }
  </style>
<?php if (function_exists("add_to_head")) { echo add_to_head(); } ?>
<link rel="stylesheet" type="text/css" href="http://www.ledprofi.com/wp-content/themes/LEDprofi/assets/css/MegaNavbar.css"/>
<link rel="stylesheet" type="text/css" href="http://www.ledprofi.com/wp-content/themes/LEDprofi/assets/css/skins/navbar-inverse-blue.css" title="inverse">
<link rel="stylesheet" type="text/css" href="http://www.ledprofi.com/wp-content/themes/LEDprofi/assets/css/animation/animation.css" title="inverse">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="http://www.ledprofi.com/wp-content/themes/LEDprofi/assets/css/hover-min.css">
<link rel="stylesheet" type="text/css" href="http://www.ledprofi.com/wp-content/themes/LEDprofi/assets/css/responsive.css" title="responsive">
<link rel="stylesheet" href="http://www.ledprofi.com/wp-content/themes/LEDprofi/bootstrap-select.min.css">

<script src="//cdn.optimizely.com/js/3214230336.js"></script>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0037/9326.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>

<script src="http://www.ledprofi.com/wp-content/themes/LEDprofi/js/bootstrap-select.min.js" type="text/javascript"/>
<script type="text/javascript">
$('.selectpicker').selectpicker();
</script>
<script type="text/javascript">

jQuery(function($) {
	$('.ledfinder').click(function (e) {
		$('select').each(function(){
	    if ($(this).val() === ''){//this assumes that your empty value is ''
	       $(this).remove();
	    }
		});
	    $ledfinderdata = $(".ledfinderform :input[value!='']").serialize();
	    $ledfinderdata_decoded = decodeURIComponent($ledfinderdata);
	    window.location.href = "http://www.ledprofi.com/gesamtes-sortiment/?" + $ledfinderdata_decoded;
	});

});

</script>
<script type="text/javascript">
jQuery(function($) {
 
	   if($('#birnen').is(':checked')) { 
	   	$('label#gu10').css('display', 'none');
	   	$('label#g9').css('display', 'none');
	   	$('label#gu53').css('display', 'none');
	   	$('label#e27').css('display', 'block');
	   	$('label#e14').css('display', 'block');
	   }

	   if($('#kerzen').is(':checked')) { 
	   	$('label#gu10').css('display', 'none');
	   	$('label#g9').css('display', 'none');
	   	$('label#gu53').css('display', 'none');
	   	$('label#e27').css('display', 'none');
	   	$('label#e14').css('display', 'block');
	   }
	   if($('#spotled').is(':checked')) { 
	   	$('label#gu10').css('display', 'block');
	   	$('label#g9').css('display', 'none');
	   	$('label#gu53').css('display', 'block');
	   	$('label#e27').css('display', 'none');
	   	$('label#e14').css('display', 'block');
	   }
	   if($('#g9').is(':checked')) { 
	   	$('label#gu10').css('display', 'none');
	   	$('label#g9').css('display', 'block');
	   	$('label#gu53').css('display', 'none');
	   	$('label#e27').css('display', 'none');
	   	$('label#e14').css('display', 'none');
	   }

$('#birnen_label').click(function() {
	   	$('label#gu10').css('display', 'none');
	   	$('label#g9').css('display', 'none');
	   	$('label#gu53').css('display', 'none');
	   	$('label#e27').css('display', 'block');
	   	$('label#e14').css('display', 'block');
	   	        $(".fass_selec>.btn-group>label>input").prop("checked", false);

        $("#fassung_egal").prop("checked", true);
        $('.fass_selec>.btn-group>label').removeClass('active');
		$('.fass_selec>.btn-group>label#fassung_egal').addClass('active');

	   });

$('#kerzen_label').click(function() {
	   	$('label#gu10').css('display', 'none');
	   	$('label#g9').css('display', 'none');
	   	$('label#gu53').css('display', 'none');
	   	$('label#e27').css('display', 'none');
	   	$('label#e14').css('display', 'block');
	   	$(".fass_selec>.btn-group>label>input").prop("checked", false);
        $("#fassung_egal").prop("checked", true);
        $('.fass_selec>.btn-group>label').removeClass('active');
		$('.fass_selec>.btn-group>label#fassung_egal').addClass('active');

	   });
$('#spots_label').click(function() {
	   	$('label#gu10').css('display', 'block');
	   	$('label#g9').css('display', 'none');
	   	$('label#gu53').css('display', 'block');
	   	$('label#e27').css('display', 'none');
	   	$('label#e14').css('display', 'block');
        $(".fass_selec>.btn-group>label>input").prop("checked", false);
        $("#fassung_egal").prop("checked", true);
        $('.fass_selec>.btn-group>label').removeClass('active');
		$('.fass_selec>.btn-group>label#fassung_egal').addClass('active');

	   });
$('#stift_label').click(function() {
	   	$('label#gu10').css('display', 'none');
	   	$('label#g9').css('display', 'block');
	   	$('label#gu53').css('display', 'none');
	   	$('label#e27').css('display', 'none');
	   	$('label#e14').css('display', 'none');
	   	$(".fass_selec>.btn-group>label>input").prop("checked", false);
        $("#fassung_egal").prop("checked", true);
        $('.fass_selec>.btn-group>label').removeClass('active');
		$('.fass_selec>.btn-group>label#fassung_egal').addClass('active');
	   });
});

</script>

<script type="text/javascript" src="http://cdn.ledprofi.com/wp-content/themes/nudie/js/bootstrap.min.js?ver=3.3.4"></script>
<script type="text/javascript">

jQuery(function($) {
	$('[data-toggle="popover"]').popover({
			html:true
	});

	$('body').on('click', function (e) {
	    $('[data-toggle="popover"]').each(function () {
	        //the 'is' for buttons that trigger popups
	        //the 'has' for icons within a button that triggers a popup
	        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
	            $(this).popover('hide');
	        }
	    });
	});	

	$('body').on('click', function (e) {
	    $('[data-toggle="popover"]').each(function () {
	            $(this).popover('hide');
	    });
	});	

	$('.infolink').click(function (e) {
	    e.stopPropagation();
	});

	$(document).click(function (e) {
	    if (($('.popover').has(e.target).length == 0) || $(e.target).is('.close')) {
	        $('.infolink').popover('hide');
	    }
	});
	$('.infolink').click(function (e) {
	    e.stopPropagation();
	});

	$(document).click(function (e) {
	    if (($('.popover').has(e.target).length == 0) || $(e.target).is('.close')) {
	        $('.infolink').popover('hide');
	    }
	});
	$('#energielabel').click(function (e) {
	    e.stopPropagation();
	});

	$(document).click(function (e) {
	    if (($('.popover').has(e.target).length == 0) || $(e.target).is('.close')) {
	        $('#energielabel').popover('hide');
	    }
	});


})



</script>

<script type="text/javascript">
	function CheckWattSelection() {
	    var w1 = document.getElementById("_sfm_birnenleistung_input_25");
	    var w2 = document.getElementById("_sfm_birnenleistung_input_35");
	    if(w1.checked) {
	        document.getElementById('alle').style.display='none';
	    }
	    else if(w2.checked) {
	        document.getElementById('alle').style.display='none';
	    }
	}
</script>

</head>
<body <?php hybrid_attr( 'body' ); ?>>
	<preheader class="menu-container" <?php hybrid_attr( 'preheader' ); ?>>

		<div class="container preheader">
				<span>
				<a href="http://www.ledprofi.com/led-lampen-testsieger/">Testsieger</a> | <a href="http://www.ledprofi.com/versandkosten/">
				<?php global $woocommerce; $user_country = $woocommerce->customer->get_country( ); if ($user_country == 'DE') { ?>
				Gratis Versand in <img class="austria-flag" alt="Deutschland" src="http://www.ledprofi.com/wp-content/uploads/2015/05/germany-e1433844655177.jpg"> ab 59,00 €
				<?php }
				else { ?>
				Gratis Versand in <img class="austria-flag" alt="Österreich" src="http://www.ledprofi.com/wp-content/uploads/2015/05/Austria.png"> ab <?php global $versandkostengrenze; echo $versandkostengrenze ?>
				<?php } ?>
				</a> | <a href="http://www.ledprofi.com/bezahlmoeglichkeiten/">Sicherer Kauf auf Rechnung</a> | <a href="/geld-zurueck-garantie/">30 Tage Geld-Zurück-Garantie</a></span>
				<span style="float:right;">
				<strong>Hotline zum Ortstarif, Mo.-Fr. von 9:00-16:00 Uhr:</strong> <a href="tel:0043720884134">+43 (0) 720 884134</a>
				</span>
		</div>

	</preheader>
	<header>
		<div class="container">
	        <nav class="navbar no-border-radius xs-height75" id="main_navbar" role="navigation">
	          
	            <div class="navbar-header">
	              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar_id">
	              <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
	              </button>
<a class="navbar-brand" href="http://www.ledprofi.com" style="color:#FDBB30;">

<div class="left_navbar_brand"><?php if(is_front_page() ) {?><h1><?php }?><img style="max-height:74px;" alt="LED-Lampen vom LEDprofi" src="http://www.ledprofi.com/wp-content/uploads/ledprofi_2.png"/><?php if(is_front_page() ) {?></h1><?php }?></div>
</a>
<a href="/warenkorb/" class="mobile 
<?php global $woocommerce; $cart_content = $woocommerce->cart->cart_contents_count;
if ($cart_content == 0) {
	echo 'gray ';
	} ?>
btn btn-block btn-warning"><i style="font-size:42px" class="fa fa-shopping-cart"></i> <i class="fa fa-angle-down"></i>
<?php
global $woocommerce; $cart_content = $woocommerce->cart->cart_contents_count;
if ($cart_content>0) { ?>
<span class="
<?php 
$content_length = strlen($cart_content); if ($content_length >=2) { echo 'twofigures ';}
?>
cart_content">
	<?php echo $cart_content ?>	
</span>
<?php } ?></a>

	            </div>
	            <div class="collapse navbar-collapse" id="navbar_id">
	              <ul class="nav navbar-nav navbar-left">
	                  <li class="dropdown-grid">
	                      <a href="/gesamtes-sortiment/" class="dropdown-toggle birne">Gesamtes Sortiment</span></a>
	                  </li>
	                  <li class="divider"></li>
	                  	  <li class="dropdown-grid">
	                      <a href="/gesamtes-sortiment/?_sfm_retro-look=1" class="dropdown-toggle kerze">Retro-Look</span></a>
	                  </li>
	                  <li class="divider"></li>
	                  <li class="dropdown-grid">
	                      <a href="/spot-leds/" class="dropdown-toggle spot">Spot-LEDs</span></a>
	                	</li>
				 <li class="divider"></li>
	        				<!-- dropdown active -->
	        				<li class="dropdown-full no-border-radius no-shadow">
	        					<a data-toggle="dropdown" href="javascript:;" class="dropdown-toggle mehr"><span class="">Mehr</span><span class="caret"></span></a>
	        					<div class="dropdown-menu no-padding">
	        						<ul>
	        							<li class="col-sm-12 dropdown-header text-center" style="padding-bottom: 10px; border-bottom: 1px solid #555;color: #333; margin: 0 -1px; width: calc(100% + 2px);">
	        							<button style="position:absolute;
right:10px;" type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
	        							<div class="row mehr_header">
	        							<div style="border-right: 1px #FDBB30 dashed;" class="col-md-4">
	        							<h4>Spezielle Lampen</h4>
	        							<ul class="mehr_list">
	        							<a style="cursor:pointer;" href="http://www.ledprofi.com/gesamtes-sortiment/?_sfm_dimmbar=1"><li>Dimmfähige Lampen</li></a>
	        							<a href="http://www.ledprofi.com/led-birnen-e27-e14/?_sfm_form=Globe"><li>LED-Globes</li></a>
	        							<a href="http://www.ledprofi.com/led-g9/"><li>G9 Leuchtmittel</li></a>
	        							<a href="http://www.ledprofi.com/gesamtes-sortiment/"><li>Gesamtes Sortiment</li></a>
	        							</ul>
	        							</div>
	        							<div style="border-right: 1px #FDBB30 dashed;" class="col-md-4">
	        							<h4>Informationen</h4>
	        							<ul class="mehr_list">
	        							<a href="/newsletter/"><li>Newsletter</li></a>
	        							<a href="/versandkosten/"><li>Versand</li></a>
	        							<a href="http://www.ledprofi.com/bezahlmoeglichkeiten/"><li>Zahlungsmethoden</li></a>
	        							</ul>
	        							</div>
	        							<div class="col-md-4">
	        							<h4>Über uns</h4>
	        							<ul class="mehr_list">
	        							<a href="/ueber-uns/"><li>Team</li></a>
	        							<a href="/kontakt/"><li>Kontakt</li></a>
	        							</ul>
	        							</div>
	        							</div>

	        							</li>
	        						</ul>
	        					</div>
	        				</li>
	              </ul>
	              <ul class="nav navbar-nav navbar-right">
	                <!-- Right side navigation-->
	                <li style="padding:0 15px;padding-right:0px">
	                  <div class="btn-group btn-block" style="margin-top: 11px; margin-bottom: 11px;">
	                    <button type="button" class="
	                    <?php global $woocommerce; $cart_content = $woocommerce->cart->cart_contents_count;
	                    if ($cart_content == 0) {
	                    	echo 'gray ';
	                    	} ?>
	                    btn btn-block btn-warning dropdown-toggle" data-toggle="dropdown"><i style="font-size:42px" class="fa fa-shopping-cart"></i> <i class="fa fa-angle-down"></i>
	                    <?php
	                    global $woocommerce; $cart_content = $woocommerce->cart->cart_contents_count;
	                    if ($cart_content>0) { ?>
	                    <span class="
	                    <?php 
	                    $content_length = strlen($cart_content); if ($content_length >=2) { echo 'twofigures ';}
	                    ?>
	                    cart_content">
	                    	<?php echo $cart_content ?>	
	                    </span>
	                   <?php } ?></button>

	                    <div class="dropdown-menu">
	                    	<?php woocommerce_get_template( 'cart/mini-cart.php', $args ); ?>
	                    </ul>
	                  </div>
	                  
	                </li>
	              </ul>
	          </div>
	        </nav>

	    </div>
	   </header>

	<main <?php hybrid_attr( 'content' ); ?>>






<script src="/wp-content/themes/LEDprofi/js/sortiment.js"></script>



		<div class="container">

			<div id="content">
				<div class="col-md-3 selection_container">
				<ul class="pre_selection">
					<li>
						<ul>
								<li style="padding-bottom:10px;" class="cat-item">
									<a style="font-size: 14px;line-height: 1.42857!important;color: #333!important;font-weight:bold" href="http://www.ledprofi.com/gesamtes-sortiment/">
										<span class="glyphicon glyphicon-arrow-left"></span> Gesamtes Sortiment
									</a>
								</li>
						</ul>
					</li>
				</ul>
				<?php echo do_shortcode( '[searchandfilter id="23293"]' ); ?>
				<div class="ledprofi_section_sidebar">
								<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/ledprofi.png" class="der_led_profi">
				<div class="info_category_header">
				<div class="float_left">
				<img class="trusted_shops" src="http://www.ledprofi.com/wp-content/uploads/2015/06/TrustedShops-rgb-Siegel_500Hpx-150x150.png">
				</div>
				<div class="float_right">
				<h4 class="no_mt ledprofi_garantie_header">Ihre LEDprofi-Garantie</h4>
				<ul class="ledprofi_garantie">
				<li>Sicher einkaufen &amp; bezahlen</li>
				<li>30 Tage Widerrufsrecht</li>
				<li>Schnelle Lieferung (1-2 Werktage)</li>
				</ul>
				</div>
				</div>
				</div>
				</div>
				<div class="col-md-9 selection_container">
				<div class="sorting-options">
				Sortierung:
				<span id="popularity">
				<span class="glyphicon glyphicon-sort"></span> Nach Beliebtheit (Standard)
				</span>
				<span id="price">
				<span class="glyphicon glyphicon-eur"></span> Nach Preis (aufsteigend)
				</span>
				<span id="lebensdauer">
				<span class="glyphicon glyphicon-leaf"></span> Nach Lebensdauer (absteigend)
				</span>
				</div>
				<div class="sortiment">
		<?php if ( have_posts() ) { ?>


			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php do_action('woocommerce_after_shop_loop'); ?>

		<?php } elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) { ?>

			<?php wc_get_template_part( 'loop/no-products-found.php' ); ?>

		<?php } ?>

		<?php do_action( 'woocommerce_pagination' ); /* woocommerce_pagination - gets pagination (10) and ordering (20) */ ?>		
		</div>
				<div class="sortiment_text_box">
			<h3>Qualitätskontrolle &amp; Produktentwicklung auf höchstem Niveau</h3>
		<p>
		Die soft-LED Leuchtmittel werden während der Produktion und vor der Auslieferung einer genauen Qualitätskontrolle unterzogen. Es stehen neben einer Ulbrichtkugel und diverse Messgeräte zu Verfügung die unsere LED-Leuchtmittel auf Stromverbrauch, Leuchtkraft, Abstrahlwinken und viele weitere Faktoren testet.
		</p>
		<p>
		Hochwertige Qualität ist eine Herzensangelegenheit!
		</p>
		<p>
		Das sehen auch die Kunden, die soft-LED Premium Produkte einsetzen. 
		</p>
		<div class="col-md-6"><ul class="references"><li> Wiener Staatsoper</li><li> Schloss Schönbrunn</li><li> Volkstheater - Wien</li><li> Schauspielhaus Graz</li><li> Staatstheater Nürnberg</li></ul></div><div class="col-md-6"><ul class="references"><li> Hofburg - Wien</li><li> Parlament - Wien</li><li> Landestheater Salzburg</li><li> Beyler Moschee - Baku</li></ul></div></div>

		</div>
			</div><!-- #content -->


		</div><!-- .container -->

<div class="breadcrumb-bar">
	<nav class="container">

	<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
	    <!-- Breadcrumb NavXT 5.2.2 -->
<span typeof="v:Breadcrumb" class="first-trail"><a rel="v:url" property="v:title" title="Gehe zu LEDprofi." href="http://www.ledprofi.com" class="home">Startseite</a></span>/<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Gehe zu Produkte." href="http://www.ledprofi.com/gesamtes-sortiment/">Gesamtes Sortiment</a></span>/<span typeof="v:Breadcrumb"><span property="v:title">G9 Leuchtmittel</span></span>	</div>

	</nav>
</div>

<?php get_footer(); // Loads the footer.php template. ?>