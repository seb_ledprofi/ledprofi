<?php
/**
 * Archive Template for the store
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

get_header(); // Loads the header.php template. 

?>

<script src="/wp-content/themes/LEDprofi/js/sortiment.js"></script>

<style>
.sf-field-post-meta-form {
	display:none!important;
}
</style>

		<div class="container">

			<div id="content">
				<div class="col-md-3">
					<ul class="pre_selection_gesamt">			
						<li class="sf-field-post-meta-form-gesamt" data-sf-field-name="_sfm_form" data-sf-field-type="post_meta" data-sf-field-input-type="radio" data-sf-meta-type="choice">
							<h4>Sortiment</h4>
							<ul class="pre_selection_gesamt">
							<a href="/led-birnen-e27-e14/?_sfm_form=Glühbirne" class="pre_selection_gesamt">
								<li class="cat-item pre_selection_li birne">
									
								 	Glühbirnen
								 	
								 </li>
							</a>
							<a href="/led-kerzen-e14/" class="pre_selection_gesamt">
								 <li class="cat-item pre_selection_li kerze">
								 	 Kerzen
								 </li>
						 	</a>
							<a href="/spot-leds/" class="pre_selection_gesamt">
								 <li class="cat-item pre_selection_li spot">
								 	 Spots
								 </li>
							</a>
							<a href="/led-birnen-e27-e14/?_sfm_form=Globe&_sfm_fassung=E27" class="pre_selection_gesamt">
								 <li class="cat-item pre_selection_li globe">
									Riesenbirne
								</li>
							</a>
							<a href="/led-birnen-e27-e14/?_sfm_form=Mini+ILLU+Birne" class="pre_selection_gesamt">
								<li class="cat-item pre_selection_li mini">
									 Mini Birnen
								</li>
							</a>
							<a href="/gesamtes-sortiment/?_sfm_fassung=GU9+%2F+G9" class="pre_selection_gesamt">
								<li class="cat-item pre_selection_li g9">
									 G9 LEDs
								</li>
							</a>
							</ul>
						</li>
					</ul>

				<?php echo do_shortcode( '[searchandfilter id="2257"]' ); ?>
				</div>

				<div class="col-md-9" style="padding-top:2px">
				<div class="row">
				<div class="col-md-5">
				<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/ledprofi.png" class="der_led_profi">
				<div class="info_category_header">
				<div class="float_left">
				<img class="trusted_shops" src="http://www.ledprofi.com/wp-content/uploads/2015/06/TrustedShops-rgb-Siegel_500Hpx-150x150.png">
				</div>
				<div class="float_right">
				<h4 class="no_mt ledprofi_garantie_header">Ihre LEDprofi-Garantie</h4>
				<ul class="ledprofi_garantie">
				<li>Sicher einkaufen &amp; bezahlen</li>
				<li>30 Tage Widerrufsrecht</li>
				<li>Schnelle Lieferung (1-2 Werktage)</li>
				</ul>
				</div>
				</div>
				</div>
				<div class="col-md-7">
				<h3>Auf der Suche nach...</h3>
				<hr />
				<div class="row" style="margin-bottom:20px;">
				<div class="col-md-6">
				<a class="gesamtes_sortiment_ctalink" href="http://www.ledprofi.com/led-birnen-e27-e14/?_sfm_form=Glühbirne&_sfm_filament=1">
				<span class="glyphicon glyphicon-chevron-right"></span>
				<div class="gesamtes_sortiment_ctanach"><span class="bignumber">Filament</span><span class="subtitle">LED Glühfaden Birnen</span></div></a>
				</div>
				<div class="col-md-6">
				<a class="gesamtes_sortiment_ctalink" href="http://www.ledprofi.com/led-kerzen-e14/?_sfm_fassung=E14&_sfm_premium=1">
				<span class="glyphicon glyphicon-chevron-right"></span>
				<div class="gesamtes_sortiment_ctanach"><span class="bignumber">Premium</span><span class="subtitle">LED Kerzenlampen</span></div></a>
				</div>
				</div>
				<hr />
				<div class="row" style="margin-bottom:20px;">
				<div class="col-md-6">
				<a class="gesamtes_sortiment_ctalink" href="http://www.ledprofi.com/spot-leds/?_sfm_premium=1">
				<span class="glyphicon glyphicon-chevron-right"></span>
				<div class="gesamtes_sortiment_ctanach"><span class="bignumber">Premium</span><span class="subtitle">Spot LEDs</span></div></a>
				</div>
				<div class="col-md-6">
				<a class="gesamtes_sortiment_ctalink" href="http://www.ledprofi.com/gesamtes-sortiment/?_sfm_dimmbar=1">
				<span class="glyphicon glyphicon-chevron-right"></span>
				<div class="gesamtes_sortiment_ctanach"><span class="bignumber">Dimmbare</span><span class="subtitle">LED Lampen</span></div></a>
				</div>
				</div>
				</div>
				</div>
				<div class="sorting-options">
				Sortierung:
				<span id="popularity">
				<span class="glyphicon glyphicon-sort"></span> Nach Beliebtheit (Standard)
				</span>
				<span id="price">
				<span class="glyphicon glyphicon-eur"></span> Nach Preis (aufsteigend)
				</span>
				<span id="lebensdauer">
				<span class="glyphicon glyphicon-leaf"></span> Nach Lebensdauer (absteigend)
				</span>
				</div>

				<div class="sortiment">
		<?php if ( have_posts() ) { ?>


			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php do_action('woocommerce_after_shop_loop'); ?>

		<?php } elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) { ?>

			<?php wc_get_template_part( 'loop/no-products-found.php' ); ?>

		<?php } ?>

		<?php do_action( 'woocommerce_pagination' ); /* woocommerce_pagination - gets pagination (10) and ordering (20) */ ?>		
		</div>
				<div class="sortiment_text_box">
			<h3>Qualitätskontrolle &amp; Produktentwicklung auf höchstem Niveau</h3>
		<p>
		Die soft-LED Leuchtmittel werden während der Produktion und vor der Auslieferung einer genauen Qualitätskontrolle unterzogen. Es stehen neben einer Ulbrichtkugel und diverse Messgeräte zu Verfügung die unsere LED-Leuchtmittel auf Stromverbrauch, Leuchtkraft, Abstrahlwinken und viele weitere Faktoren testet.
		</p>
		<p>
		Hochwertige Qualität ist eine Herzensangelegenheit!
		</p>
		<p>
		Das sehen auch die Kunden, die soft-LED Premium Produkte einsetzen. 
		</p>
		<div class="col-md-6"><ul class="references"><li> Wiener Staatsoper</li><li> Schloss Schönbrunn</li><li> Volkstheater - Wien</li><li> Schauspielhaus Graz</li><li> Staatstheater Nürnberg</li></ul></div><div class="col-md-6"><ul class="references"><li> Hofburg - Wien</li><li> Parlament - Wien</li><li> Landestheater Salzburg</li><li> Beyler Moschee - Baku</li></ul></div></div>

		</div>
			</div><!-- #content -->


		</div><!-- .container -->
<?php get_footer(); // Loads the footer.php template. ?>