<?php
/**
 * Archive Template for the store
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

get_header(); // Loads the header.php template. ?>




		<div class="container">

			<div id="content">
				<div class="col-md-3">
				<?php echo do_shortcode( '[searchandfilter id="2257"]' ); ?>
				</div>
				<div class="col-md-9" style="padding-top:2px">

		<?php if ( have_posts() ) { ?>



			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php do_action('woocommerce_after_shop_loop'); ?>

		<?php } elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) { ?>

			<?php wc_get_template_part( 'loop/no-products-found.php' ); ?>

		<?php } ?>

		<?php do_action( 'woocommerce_pagination' ); /* woocommerce_pagination - gets pagination (10) and ordering (20) */ ?>		
		</div>
			</div><!-- #content -->

		<?php get_sidebar( 'woocommerce' ); // Loads the sidebar-woocommerce.php template. ?>

		</div><!-- .container -->

<?php get_footer(); // Loads the footer.php template. ?>