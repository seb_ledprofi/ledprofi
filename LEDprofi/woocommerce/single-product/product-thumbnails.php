<?php
/**
 * Single Product Thumbnails
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product, $woocommerce;

$attachment_ids = $product->get_gallery_attachment_ids();

if ( $attachment_ids ) {
		$loop = 0;
		$columns = apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
		?>
		<div class="thumbnails <?php echo 'columns-' . $columns; ?>"><?php

		foreach ( $attachment_ids as $attachment_id ) {

			$classes = array( 'zoom' );

			if ( $loop == 0 || $loop % $columns == 0 )
				$classes[] = 'first';

			if ( ( $loop + 1 ) % $columns == 0 )
				$classes[] = 'last';

			$image_link = wp_get_attachment_url( $attachment_id );

			if ( ! $image_link )
				continue;

			$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ) );
			$image_class = esc_attr( implode( ' ', $classes ) );
			$image_title = esc_attr( get_the_title( $attachment_id ) );
			if ( $product->is_type( 'simple' ) ) {
			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="%s" class="%s thumbnail" title="%s" data-rel="prettyPhoto[product-gallery]">%s</a>', $image_link, $image_class, $image_title, $image ), $attachment_id, $post->ID, $image_class );
			}
			
			$loop++;
		}

	?>
	<button id="energielabel" style="padding-left:49px" data-placement="top" data-toggle="popover" title="<img style='height:400px;width:215px;' src='<?php $effizienz = get_field( "energielabel" ); if ($effizienz == "A1") { ?>http://www.ledprofi.com/wp-content/uploads/2015/05/energy_a1.jpg<?php } if ($effizienz == "A2") {?>http://www.ledprofi.com/wp-content/uploads/2015/05/energie_a2.jpg<?php } if ($effizienz == "A3") {?>http://www.ledprofi.com/wp-content/uploads/2015/05/energie_a.jpg<?php } if ($effizienz == "A") {?>http://www.ledprofi.com/wp-content/uploads/2015/05/energie_a.jpg<?php } ?>' /><span class='kwh_1000h big_label'><?php $kwh_1000h = get_field( "leistungsaufnahme_watt_genau" ); echo $kwh_1000h; ?> KWh / 1000h</span>" class="popup-marker thumbnail last energy">
			<?php $energielabel = get_field( "energielabel" ); if ($energielabel == "A1") {?><div id="energyEfficiencySprite" class="a-section a-spacing-none energy_efficiency_sprite A1">			</div>
<?php } else {}; ?>
			<?php if ($energielabel == "A2") {?><div id="energyEfficiencySprite" class="a-section a-spacing-none energy_efficiency_sprite A2">			</div>
<?php } else {}; ?>
			<?php if ($energielabel == "A3") {?><div id="energyEfficiencySprite" class="a-section a-spacing-none energy_efficiency_sprite A3">			</div>
<?php } else {}; ?>
			<?php if ($energielabel == "A") {?><div id="energyEfficiencySprite" class="a-section a-spacing-none energy_efficiency_sprite A">			</div>
<?php } else {} ?>
			<span class="kwh_1000h"><?php $kwh_1000h = get_field( "leistungsaufnahme_watt_genau" ); echo $kwh_1000h; ?> KWh / 1000h</span>
</button>
	</div>
	    <?php
        if( $product->is_type( 'bundle' )) {?>
	<p>Unsere Packs werden in Großpackungen geliefert. Daher können wir bessere Preise als bei der Bestellung von Einzelverpackungen anbieten.</p>
<?php } ?>

	<?php
}