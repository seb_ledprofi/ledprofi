<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>
<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
	<table>
		<tbody>
			<?php 
			$uvp = get_field( "uvp" ); 
			$uvpspecial = number_format((float)$uvp, 2, ',', '');

			$stueckzahl = get_field ( "stueckzahl" );
			if ($stueckzahl != 1) {?>
			<tr>
				<td class="preis product_left">Preis:</td>
				<td class="preis-value"><?php $unserpreis = $product->get_price_html(); echo $unserpreis ?> <span class="savings">(<?php 
					$uvp2 = str_replace(',', '', $uvpspecial); 
					$uvp3 = $uvp2; 
					$uvp4 = $uvp3/100; 
					$uvp2special = number_format((float)$uvp4, 2, ',', '');
				 $unserpreis2 = $int = ereg_replace("[^0-9]", "", $unserpreis) ; $gespart = $uvp3 - $unserpreis2; $gespart2 = $gespart / $uvp3; $gespart3 = $gespart2 * 100; $gespart_smart = number_format((float)$gespart/100, 2, ',', ''); echo $gespart_smart ?> € gespart - UVP: <?php echo $uvp2special ?> €)</span></td>
			</tr>
			<?php } else {?>
			<tr>
				<td class="preis product_left">Preis:</td>
				<td class="preis-value"><?php $unserpreis = $product->get_price_html(); echo $unserpreis ?> <span class="savings">(<?php $uvp2 = str_replace(',', '', $uvpspecial);
					$uvp3 = $uvp2*$stueckzahl; 
					$uvp4 = $uvp3/100; 
					$uvp2special = number_format((float)$uvp4, 2, ',', '');
				 $unserpreis2 = $int = ereg_replace("[^0-9]", "", $unserpreis) ; $gespart = $uvp3 - $unserpreis2; $gespart2 = $gespart / $uvp3; $gespart3 = $gespart2 * 100; echo number_format((float)$gespart3, 0, ',', ''); ?>% gespart - UVP: <?php echo $uvp2special ?> €)</span></td>
			</tr><?php
				}

?>
			<tr>
				<td></td>
				<td class="mwst">Alle Preisangaben inkl. MwSt.</td>
			</tr>
		<tbody>
	</table>

	<meta itemprop="price" content="<?php echo $product->get_price_including_tax(); ?>" />
	<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />

</div>
