<?php
/**
 * Simple product add to cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product;

if ( ! $product->is_purchasable() ) return;
?>

<?php
	// Availability
	$availability = $product->get_availability();
	$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>';

	echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability['availability'], $product );
?>

<?php if ( $product->is_in_stock() ) : ?>

	<?php do_action('woocommerce_before_add_to_cart_form'); ?>
	<form class="cart" method="post" enctype='multipart/form-data'>
	<p class="menge">Menge:</p>
	 	<?php do_action('woocommerce_before_add_to_cart_button'); ?>

	 	<?php
	 		if ( ! $product->is_sold_individually() )
	 			woocommerce_quantity_input( array(
	 				'min_value' => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
	 				'max_value' => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product )
	 			) );
	 	?>

	 	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />
	<?php $coming_soon_1 = get_field("coming_soon_1"); $coming_soon_2 = get_field("coming_soon_2"); $verfugbar = get_field("verfugbar"); if ($verfugbar or $coming_soon_2 or $coming_soon_1) {?>

	 	<button type="submit" id="in_den_warenkorb" class="single_add_to_cart_button btn btn-warning"><span class="bundle-left"><i style="font-size:32px" class="fa fa-shopping-cart"></i></span><span class="bundle-right">In den <br />Einkaufswagen</span></button>
<?php } else {
	echo '<a href="/kontakt/" class="single_add_to_cart_button btn btn-warning">Jetzt anfragen</a>';  }?>
	 	<?php do_action('woocommerce_after_add_to_cart_button'); ?>

	</form>

	<?php do_action('woocommerce_after_add_to_cart_form'); ?>

<?php endif; ?>