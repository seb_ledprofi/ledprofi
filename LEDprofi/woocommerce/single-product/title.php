<?php
/**
 * Single Product title
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?>
<div class="page-header">
	<h1 itemprop="name" class="product_title entry-title">
	<?php global $product; if( $product->is_type( 'bundle' ) ){?>
	<span class="bundle_header"><?php $stueckzahl = get_field("stueckzahl"); echo $stueckzahl; ?>er-Pack:</span><br /><?php }?>
	<?php $bezeichnung = get_field( "bezeichnung" ); echo $bezeichnung ?> <?php $watt = get_field( "watt" ); echo $watt ?>W<?php $ausfuehrung = get_field("ausfuehrung"); if ($ausfuehrung) {?><br /><span style="font-size:20px;font-weight:normal;">mit <?php echo $ausfuehrung;?></span><?php } ?><br />
	<div class="title_wattage"><span class="small_title_wattage">ersetzt<br /></span><span class="title_wattage_number"><?php $birnenleistung = get_field( "birnenleistung" )?><?php  echo $birnenleistung?></span><br />Watt</div><div class="title_wattage"><span class="small_title_wattage">passt für<br/></span><span class="title_wattage_number"><?php $fassung = get_field( "fassung" )?><?php  echo $fassung?></span><br />Sockel</div><?php $glas = get_field( "glas" ); $form = get_field("form"); if ($glas == "matt" && $form !="Spot") { ?><div class="title_wattage"> <br /><span class="title_wattage_number"><?php echo $glas;?></span></div><?php } ?>
	<?php $dimmbar = get_field("dimmbar"); if ($dimmbar == TRUE) { ?><div class="title_wattage"> <br /><span class="title_wattage_number">dimmbar</span></div><?php } ?>

	<div class="title_wattage final"><?php $lichtfarbe = get_field( "lichtfarbe-beschreibung" )?> <br />
<span class="farbtemperatur <?php

 if ($lichtfarbe == "warmweiß") { echo "warmweiss"; }?>
<?php if ($lichtfarbe == "naturweiß") {echo "naturweiss"; }?>
<?php if ($lichtfarbe == "supersoft warmweiß" or $lichtfarbe == "extra warmweiß" or $lichtfarbe == "extrawarmweiß"  ) { echo "extrawarmweiss"; }?>"><?php  echo $lichtfarbe?></span>
</div></h1>
</div>
<?php $header_length = strlen($bezeichnung); if ($header_length >=34) {?><style>h1.product_title.entry-title {font-size:24px!important;}</style><?php }?>
