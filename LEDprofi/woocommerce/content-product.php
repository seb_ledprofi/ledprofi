<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 3 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
$classes = 'hvr-fade';


if ( ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] == 0 || $woocommerce_loop['columns'] == 1) {
	$classes .= ' first ';
	echo '<div class="row">';
	$woocommerce_loop['close_row'] = true ;
}
if ( $woocommerce_loop['loop'] % $woocommerce_loop['columns'] == 0 ) {
	$classes .= ' last ';
	$woocommerce_loop['close_row'] = false;
}
$form = get_field("form");
if ( $form == "Mini ILLU Birne") {
	$classes .= ' mini ';
}
?>
<div 
<?php if ( is_product() ) {  
?>
class="col-xs-12 col-sm-3">
<?php }
else {
	?>
class="col-xs-12 col-sm-4">
<li <?php post_class( $classes ); ?>>
	<?php
} ?>

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

	<a href="<?php the_permalink(); ?>">
<h3 style="margin-top:6px;" class="product_title_cat"><?php $bezeichnung = get_field( "bezeichnung_sortiment" ); echo $bezeichnung ?> <?php $watt = get_field( "watt" ); echo $watt ?>W</h3>
<span class="sockel_wattersatz"><?php $fassung = get_field( "fassung" )?><?php  echo $fassung?> - wie <?php $birnenleistung = get_field( "birnenleistung" )?><?php  echo $birnenleistung?> Watt - <?php $lumen = get_field( "lumen_l70" ); echo $lumen ?> Lm</span>
<div class="price_color">
<?php $lichtfarbe = get_field( "lichtfarbe-beschreibung" )?>
<span style="margin-left:0px!important" class="farbtemperatur <?php

 if ($lichtfarbe == "warmweiß") { echo "warmweiss"; }?>
<?php if ($lichtfarbe == "naturweiß") {echo "naturweiss"; }?>
<?php if ($lichtfarbe == "supersoft warmweiß" or $lichtfarbe == "extra warmweiß" or $lichtfarbe == "extrawarmweiß"  ) { echo "extrawarmweiss"; }?>"><?php  echo $lichtfarbe?></span>
<span style="vertical-align:middle;" class="price"><?php echo $product->get_price_html(); ?><span style="display:block;font-size:12px;font-weight:normal;">Sie sparen <strong>
<?php
$uvp = get_field( "uvp" );
$unserpreis = $product->get_price_html();
$uvpspecial = number_format((float)$uvp, 2, ',', '');
$uvp2 = str_replace(',', '', $uvpspecial); 
$uvp3 = $uvp2; 
$uvp4 = $uvp3/100; 
$uvp2special = number_format((float)$uvp4, 2, ',', '');

$unserpreis2 = $int = ereg_replace("[^0-9]", "", $unserpreis) ;
 $gespart = $uvp2 - $unserpreis2; $gespart2 = $gespart / $uvp2; $prozent_ersparnis = $gespart2 * 100; $prozent_ersparnis_format = number_format((float)$prozent_ersparnis, 1, ',', '');
 echo $prozent_ersparnis_format;
?>
%</strong></span></span>
</div>

<div class="product_cat_info">
<span class="lebensdauer_cat_info">
hält ca. <strong><?php $lifetime = get_field( "lebensdauer_in_jahren_4h_pro_tag" ); echo number_format((float)$lifetime, 0, ',', '');  ?> Jahre</strong> - <?php $lebensdauer_in_stunden = get_field( "lebensdauer_in_stunden" ); echo number_format($lebensdauer_in_stunden, 0, ',', '.'); ?> Stunden</span>

</div>
<div class="product_cat_info">
<span class="lumen_cat_info">
<?php $lichtfarbekelvin = get_field( "lichtfarbe" )?><?php  echo $lichtfarbekelvin?>K<?php $form = get_field("form"); $ausfuehrung = get_field("ausfuehrung"); $fassung = get_field("fassung"); if ($ausfuehrung && $fassung != "GU9 / G9") {
 	echo ' - ';
	echo $ausfuehrung;
	} elseif ($form != "G9 Leuchtmittel") { $glas = get_field( "glas" ); echo ' - '; echo $glas; echo 'es Glas'; } ?>

</span>
<span class="lebensdauer_cat_info"><?php $abstrahlwinkel = get_field( "abstrahlwinkel" ); echo $abstrahlwinkel ?>&deg; Abstrahlwinkel - <?php $farbwiedergabe = get_field( "farbwiedergabewert" ); echo $farbwiedergabe ?></span>

</div>
<?php 
	$dimmbar = get_field("dimmbar");
	if ($dimmbar) {?>
	<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/7c05d4f3fa.png" class="sortiment_label_1" />
	<?php }
	$premium = get_field("premium");
	if ($premium && $dimmbar == FALSE) {?>
	<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/premium.png" class="sortiment_label_1" />
	<?php }
	if ($premium && $dimmbar && $form !="Spot") {?>
	<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/premium.png" class="sortiment_label_2" />
	<?php }
	$durchmesser = get_field("durchmesser");
	if ($durchmesser == "125" && $premium && $form == "Globe") {?>
	<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/unnamed.png" class="sortiment_label_3" />
	<?php }
	if ($durchmesser == "120" && $form == "Globe") {?>
	<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/12cmlabel.png" class="sortiment_label_1" />
	<?php }
	if ($durchmesser == "95" && $form == "Globe") {?>
	<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/95cmlabel.png" class="sortiment_label_1" />
	<?php }
	$testsieger_technik = get_field("testsieger");
	if ($testsieger_technik && $form != "Globe") {?>
	<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/839a037ea3.png" class="sortiment_label_3" />
	<?php }
	if ($testsieger_technik && $form == "Globe") {?>
	<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/839a037ea3.png" class="sortiment_label_4" />
	<?php }
	if ($premium && $dimmbar && $form =="Spot" && $testsieger_technik) {?>
	<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/premium.png" class="sortiment_label_4" />
	<?php }
	?>
	<?php $fassung = get_field( "fassung" );
	if ($fassung == "E14") { ?>
	<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/e14.jpg" class="fassung_image attachment-shop_catalog wp-post-image">
	<?php }
	if ($fassung == "E27") { ?>
	<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/e27.jpg" class="fassung_image attachment-shop_catalog wp-post-image">
	<?php }
	if ($fassung == "GU10") { ?>
	<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/gu10.jpg" class="fassung_image attachment-shop_catalog wp-post-image">
	<?php }
	if ($fassung == "GU5.3 / MR16") { ?>
	<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/gu53.jpg" class="fassung_image attachment-shop_catalog wp-post-image">
	<?php }
	if ($fassung == "GU9 / G9") { ?>
	<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/g9.jpg" class="fassung_image attachment-shop_catalog wp-post-image">
	<?php }?>
	<span class="fassung_label_category">
	<?php
	echo $fassung;
	?>
	</span>
		<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );
		?>
<div class="product_cat_info" style="border-top:0px transparent;
padding-top:0px;">
<span class="lebensdauer_cat_info">Designed in <img src="http://www.ledprofi.com/wp-content/uploads/2015/05/Austria.png" class="austria-flag"> <span class="eu_label">EU</span></span>
<span class="lebensdauer_cat_info">Garantie: <?php $garantie = get_field( "garantie_(monate)" )/12; echo $garantie; ?> Jahre</span>
</div>


		<?php
			/**
			 * woocommerce_after_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_template_loop_rating - 5
			 * @hooked woocommerce_template_loop_price - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item_title' );
		?>

	</a>
<?php $coming_soon_1 = get_field("coming_soon_1"); $coming_soon_2 = get_field("coming_soon_2"); $verfugbar = get_field("verfugbar"); 
 if ($verfugbar) { ?>
<?php
		/**
		 * woocommerce_after_shop_loop_item hook
		 *
		 * @hooked woocommerce_template_loop_add_to_cart - 10
		 */
		do_action( 'woocommerce_after_shop_loop_item' ); 
	?>
<span class="lieferstatus">
Auf Lager - Lieferung in 1-2 Werktagen
</span>
<?php }
if ($coming_soon_1) {?>
<?php

		/**
		 * woocommerce_after_shop_loop_item hook
		 *
		 * @hooked woocommerce_template_loop_add_to_cart - 10
		 */
		do_action( 'woocommerce_after_shop_loop_item' ); 
	?>
<span class="lieferstatus">
In Produktion - Lieferung in 1-2 Wochen
</span>
<?php }
if ($coming_soon_2) {?>
<?php

		/**
		 * woocommerce_after_shop_loop_item hook
		 *
		 * @hooked woocommerce_template_loop_add_to_cart - 10
		 */
		do_action( 'woocommerce_after_shop_loop_item' ); 
	?>
<span class="lieferstatus">
In Produktion - Lieferung in 6-8 Wochen</span>
<?php }
if ($coming_soon_2 == FALSE && $coming_soon_1 == FALSE && $verfugbar == FALSE) {?>
<hr style="margin:10px 0px;" />
<p style="text-align:center;color:#333!important;font-size:12px;">Leider ist dieses Produkt gerade nicht erhältlich. Vorbestellung möglich.</p>
<?php } ?>

<?php if ( is_product() ) { }
else {
	?>
</li>

	<?php
} ?>




</div>
<?php

if ( ($woocommerce_loop['loop'])%2 == 0 )
	echo '<div class="clearfix visible-xs"></div>';

if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	echo '</div><!-- .row -->';