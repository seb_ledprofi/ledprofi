<?php
/**
 * Archive Template for the store
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

get_header(); // Loads the header.php template. 

?>

<style>
ul._sfm_fassung > li.cat-item._E14 {
	margin-right:0px!important;
}
</style>
<script src="/wp-content/themes/LEDprofi/js/sortiment.js"></script>
		<div class="container">

			<div id="content">
				<div class="col-md-3 selection_container">
				<ul class="pre_selection">
					<li>
						<ul>
								<li style="padding-bottom:10px;" class="cat-item">
									<a style="font-size: 14px;line-height: 1.42857!important;color: #333!important;font-weight:bold" href="http://www.ledprofi.com/gesamtes-sortiment/">
										<span class="glyphicon glyphicon-arrow-left"></span> Gesamtes Sortiment
									</a>
								</li>
						</ul>
					</li>
				</ul>
				<?php echo do_shortcode( '[searchandfilter id="2283"]' ); ?>
				<div class="ledprofi_section_sidebar">
								<img src="http://www.ledprofi.com/wp-content/uploads/2015/06/ledprofi.png" class="der_led_profi">
				<div class="info_category_header">
				<div class="float_left">
				<img class="trusted_shops" src="http://www.ledprofi.com/wp-content/uploads/2015/06/TrustedShops-rgb-Siegel_500Hpx-150x150.png">
				</div>
				<div class="float_right">
				<h4 class="no_mt ledprofi_garantie_header">Ihre LEDprofi-Garantie</h4>
				<ul class="ledprofi_garantie">
				<li>Sicher einkaufen &amp; bezahlen</li>
				<li>30 Tage Widerrufsrecht</li>
				<li>Schnelle Lieferung (1-2 Werktage)</li>
				</ul>
				</div>
				</div>
				</div>
				</div>
				<div class="col-md-9 selection_container">
				<div class="sorting-options">
				Sortierung:
				<span id="popularity">
				<span class="glyphicon glyphicon-sort"></span> Nach Beliebtheit (Standard)
				</span>
				<span id="price">
				<span class="glyphicon glyphicon-eur"></span> Nach Preis (aufsteigend)
				</span>
				<span id="lebensdauer">
				<span class="glyphicon glyphicon-leaf"></span> Nach Lebensdauer (absteigend)
				</span>
				</div>
				<div class="sortiment">
		<?php if ( have_posts() ) { ?>



			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php do_action('woocommerce_after_shop_loop'); ?>

		<?php } elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) { ?>

			<?php wc_get_template_part( 'loop/no-products-found.php' ); ?>

		<?php } ?>

		<?php do_action( 'woocommerce_pagination' ); /* woocommerce_pagination - gets pagination (10) and ordering (20) */ ?>		
		</div>
				<div class="sortiment_text_box">
			<h3>Qualitätskontrolle &amp; Produktentwicklung auf höchstem Niveau</h3>
		<p>
		Die soft-LED Leuchtmittel werden während der Produktion und vor der Auslieferung einer genauen Qualitätskontrolle unterzogen. Es stehen neben einer Ulbrichtkugel und diverse Messgeräte zu Verfügung die unsere LED-Leuchtmittel auf Stromverbrauch, Leuchtkraft, Abstrahlwinken und viele weitere Faktoren testet.
		</p>
		<p>
		Hochwertige Qualität ist eine Herzensangelegenheit!
		</p>
		<p>
		Das sehen auch die Kunden, die soft-LED Premium Produkte einsetzen. 
		</p>
		<div class="col-md-6"><ul class="references"><li> Wiener Staatsoper</li><li> Schloss Schönbrunn</li><li> Volkstheater - Wien</li><li> Schauspielhaus Graz</li><li> Staatstheater Nürnberg</li></ul></div><div class="col-md-6"><ul class="references"><li> Hofburg - Wien</li><li> Parlament - Wien</li><li> Landestheater Salzburg</li><li> Beyler Moschee - Baku</li></ul></div></div>

		</div>
			</div><!-- #content -->


		</div><!-- .container -->

<div class="breadcrumb-bar">
	<nav class="container">

	<div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
	    <!-- Breadcrumb NavXT 5.2.2 -->
<span typeof="v:Breadcrumb" class="first-trail"><a rel="v:url" property="v:title" title="Gehe zu LEDprofi." href="http://www.ledprofi.com" class="home">Startseite</a></span>/<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" title="Gehe zu Produkte." href="http://www.ledprofi.com/gesamtes-sortiment/">Gesamtes Sortiment</a></span>/<span typeof="v:Breadcrumb"><span property="v:title">Spot-LEDs</span></span>	</div>

	</nav>
</div>

<?php get_footer(); // Loads the footer.php template. ?>