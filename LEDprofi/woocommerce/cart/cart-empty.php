<?php
/**
 * Empty cart page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

wc_print_notices();

?>

<div class="alert alert-warning"><?php _e( 'Your cart is currently empty.', 'woocommerce' ) ?></div>

<?php do_action('woocommerce_cart_is_empty'); ?>

<a class="btn btn-default" href="http://www.ledprofi.com"><?php _e( '&larr; Zurück zum Shop', 'woocommerce' ) ?></a>