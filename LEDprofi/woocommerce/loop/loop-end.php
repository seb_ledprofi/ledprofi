<?php
/**
 * Product Loop End
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
global $woocommerce_loop;
if ($woocommerce_loop['close_row']) echo '</div><!--row -->';
?>
</div>