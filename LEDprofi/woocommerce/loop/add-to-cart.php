<?php
/**
 * Loop Add to Cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<?php 
/**
 * 
 * $verfugbar = get_field("verfugbar");
 *  if ($verfugbar) { ?>
 * 
 * <span class="delivery_status glyphicon glyphicon-bookmark"></span>
 * <?php }
 * $coming_soon_1 = get_field("coming_soon_1");
 *  if ($coming_soon_1) { ?>
 * 
 * <span class="delivery_status coming_soon_1 glyphicon glyphicon-bookmark"></span>
*/
?>

<span class="delivery_status glyphicon glyphicon-bookmark"></span>

<?php
global $product;

echo apply_filters( 'woocommerce_loop_add_to_cart_link',
	sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" style="padding:0px!important;" data-quantity="%s" class="button btn btn-primary add_to_cart_button  %s product_type_%s"><span class="bundle-left"><i style="font-size:32px" class="fa fa-shopping-cart"></i></span>%s</a>',
		esc_url( $product->add_to_cart_url() ),
		esc_attr( $product->id ),
		esc_attr( $product->get_sku() ),
		esc_attr( isset( $quantity ) ? $quantity : 1 ),
		$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
		esc_attr( $product->product_type ),
		esc_html( $product->add_to_cart_text() )
	),
$product );
