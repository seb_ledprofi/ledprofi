<?php
/**
 * File Security Check
 */
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}

if ( has_nav_menu( 'primary' ) ) { ?>

	<div class="collapse navbar-collapse navbar-responsive-collapse">

		<?php 

		wp_nav_menu(
			array(
				'theme_location'	=> 'primary',
				'container'			=> 'nav',
				'container_id'		=> 'menu-primary',
				'container_class'	=> 'menu',
				'menu_id'			=> 'menu-primary-items',
				'menu_class'		=> 'nav navbar-nav menu-items',
				'fallback_cb'		=> '',
				'depth'				=> 2,
				'walker' => new Bootstrap_Walker_Nav_Menu()
			)
		);

		?>

		<div class="navbar-form navbar-right">
			<span class="glyphicon glyphicon-shopping-cart headercart"></span>
		</div>

	</div><!-- .nav-collapse -->


<?php }