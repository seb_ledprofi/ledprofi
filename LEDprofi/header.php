<?php
/**
 * File Security Check
 */
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js lt-ie9 lt-ie8 ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js lt-ie9 ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>

<?php wp_head(); // wp_head ?>
<?php if (function_exists("add_to_head")) { echo add_to_head(); } ?>
<link rel="stylesheet" type="text/css" href="http://www.ledprofi.com/wp-content/themes/LEDprofi/assets/css/MegaNavbar.css"/>
<link rel="stylesheet" type="text/css" href="http://www.ledprofi.com/wp-content/themes/LEDprofi/assets/css/skins/navbar-inverse-blue.css" title="inverse">
<link rel="stylesheet" type="text/css" href="http://www.ledprofi.com/wp-content/themes/LEDprofi/assets/css/animation/animation.css" title="inverse">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="http://www.ledprofi.com/wp-content/themes/LEDprofi/assets/css/hover-min.css">
<link rel="stylesheet" type="text/css" href="http://www.ledprofi.com/wp-content/themes/LEDprofi/assets/css/responsive.css" title="responsive">
<link rel="stylesheet" href="http://www.ledprofi.com/wp-content/themes/LEDprofi/bootstrap-select.min.css">

<script src="//cdn.optimizely.com/js/3214230336.js"></script>

<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0037/9326.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>

<script src="http://www.ledprofi.com/wp-content/themes/LEDprofi/js/bootstrap-select.min.js" type="text/javascript"/>
<script type="text/javascript">
$('.selectpicker').selectpicker();
</script>
<script type="text/javascript">

jQuery(function($) {
	$('.ledfinder').click(function (e) {
		$('select').each(function(){
	    if ($(this).val() === ''){//this assumes that your empty value is ''
	       $(this).remove();
	    }
		});
	    $ledfinderdata = $(".ledfinderform :input[value!='']").serialize();
	    $ledfinderdata_decoded = decodeURIComponent($ledfinderdata);
	    window.location.href = "http://www.ledprofi.com/gesamtes-sortiment/?" + $ledfinderdata_decoded;
	});

});

</script>
<script type="text/javascript">
jQuery(function($) {
 
	   if($('#birnen').is(':checked')) { 
	   	$('label#gu10').css('display', 'none');
	   	$('label#g9').css('display', 'none');
	   	$('label#gu53').css('display', 'none');
	   	$('label#e27').css('display', 'block');
	   	$('label#e14').css('display', 'block');
	   	$('label#e14').css('border-bottom-right-radius', '4px');
	   	$('label#e14').css('border-top-right-radius', '4px');
	   }

	   if($('#kerzen').is(':checked')) { 
	   	$('label#gu10').css('display', 'none');
	   	$('label#g9').css('display', 'none');
	   	$('label#gu53').css('display', 'none');
	   	$('label#e27').css('display', 'none');
	   	$('label#e14').css('display', 'block');
	   }
	   if($('#spotled').is(':checked')) { 
	   	$('label#gu10').css('display', 'block');
	   	$('label#g9').css('display', 'none');
	   	$('label#gu53').css('display', 'block');
	   	$('label#e27').css('display', 'none');
	   	$('label#e14').css('display', 'block');
	   }
	   if($('#g9').is(':checked')) { 
	   	$('label#gu10').css('display', 'none');
	   	$('label#g9').css('display', 'block');
	   	$('label#gu53').css('display', 'none');
	   	$('label#e27').css('display', 'none');
	   	$('label#e14').css('display', 'none');
	   }

$('#birnen_label').click(function() {
	   	$('label#fassung_egal').css('display', 'block');
	   	$('label#gu10').css('display', 'none');
	   	$('label#g9').css('display', 'none');
	   	$('label#gu53').css('display', 'none');
	   	$('label#e27').css('display', 'block');
	   	$('label#e14').css('display', 'block');
	   	$(".fass_selec>.btn-group>label>input").prop("checked", false);
        $("#fassung_egal").prop("checked", true);
        $('.fass_selec>.btn-group>label').removeClass('active');
		$('.fass_selec>.btn-group>label#fassung_egal').addClass('active');
	   	$('label#e14').css('border-radius', '0px');
	   	$('label#e14').css('border-bottom-right-radius', '4px');
	   	$('label#e14').css('border-top-right-radius', '4px');
	   	$('label#g9').css('border-radius', '0px');

	   });

$('#kerzen_label').click(function() {
	   	$('label#fassung_egal').css('display', 'none');
	   	$('label#gu10').css('display', 'none');
	   	$('label#g9').css('display', 'none');
	   	$('label#gu53').css('display', 'none');
	   	$('label#e27').css('display', 'none');
	   	$('label#e14').css('display', 'block');
        $("#fassung_egal").prop("checked", false);
	   	$(".fass_selec>.btn-group>label>input").prop("checked", false);
	   	$(".fass_selec>.btn-group>label#e14>input").prop("checked", true);
        $('.fass_selec>.btn-group>label').removeClass('active');
		$('.fass_selec>.btn-group>label#fassung_egal').removeClass('active');
        $('.fass_selec>.btn-group>label#e14').addClass('active');
	   	$('label#e14').css('border-radius', '4px');
	   	$('label#e14').css('border-bottom-right-radius', '4px');
	   	$('label#e14').css('border-top-right-radius', '4px');

	   });
$('#spots_label').click(function() {
	   	$('label#fassung_egal').css('display', 'block');
	   	$('label#gu10').css('display', 'block');
	   	$('label#g9').css('display', 'none');
	   	$('label#gu53').css('display', 'block');
	   	$('label#e27').css('display', 'none');
	   	$('label#e14').css('display', 'block');
        $(".fass_selec>.btn-group>label>input").prop("checked", false);
        $("#fassung_egal").prop("checked", true);
        $('.fass_selec>.btn-group>label').removeClass('active');
		$('.fass_selec>.btn-group>label#fassung_egal').addClass('active');
	   	$('label#e14').css('border-radius', '0px');
	   	$('label#g9').css('border-radius', '0px');
	   	$('label#e14').css('border-bottom-right-radius', '0px');
	   	$('label#e14').css('border-top-right-radius', '0px');

	   });
$('#stift_label').click(function() {
	   	$('label#fassung_egal').css('display', 'none');
	   	$('label#gu10').css('display', 'none');
	   	$('label#g9').css('display', 'block');
	   	$('label#gu53').css('display', 'none');
	   	$('label#e27').css('display', 'none');
	   	$('label#e14').css('display', 'none');
	   	$(".fass_selec>.btn-group>label>input").prop("checked", false);
        $("#fassung_egal").prop("checked", false);
	   	$(".fass_selec>.btn-group>label>input").prop("checked", false);
	   	$(".fass_selec>.btn-group>label#g9>input").prop("checked", true);
        $('.fass_selec>.btn-group>label').removeClass('active');
		$('.fass_selec>.btn-group>label#fassung_egal').removeClass('active');
        $('.fass_selec>.btn-group>label#g9').addClass('active');
       	$('label#e14').css('border-radius', '0px');
	   	$('label#g9').css('border-radius', '4px');
	   });
});

</script>


<script type="text/javascript">

jQuery(function($) {
	$('[data-toggle="popover"]').popover({
			html:true
	});

	$('body').on('click', function (e) {
	    $('[data-toggle="popover"]').each(function () {
	        //the 'is' for buttons that trigger popups
	        //the 'has' for icons within a button that triggers a popup
	        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
	            $(this).popover('hide');
	        }
	    });
	});	

	$('body').on('click', function (e) {
	    $('[data-toggle="popover"]').each(function () {
	            $(this).popover('hide');
	    });
	});	

	$('.infolink').click(function (e) {
	    e.stopPropagation();
	});

	$(document).click(function (e) {
	    if (($('.popover').has(e.target).length == 0) || $(e.target).is('.close')) {
	        $('.infolink').popover('hide');
	    }
	});
	$('.infolink').click(function (e) {
	    e.stopPropagation();
	});

	$(document).click(function (e) {
	    if (($('.popover').has(e.target).length == 0) || $(e.target).is('.close')) {
	        $('.infolink').popover('hide');
	    }
	});
	$('#energielabel').click(function (e) {
	    e.stopPropagation();
	});

	$(document).click(function (e) {
	    if (($('.popover').has(e.target).length == 0) || $(e.target).is('.close')) {
	        $('#energielabel').popover('hide');
	    }
	});


})



</script>

<script type="text/javascript">
	function CheckWattSelection() {
	    var w1 = document.getElementById("_sfm_birnenleistung_input_25");
	    var w2 = document.getElementById("_sfm_birnenleistung_input_35");
	    if(w1.checked) {
	        document.getElementById('alle').style.display='none';
	    }
	    else if(w2.checked) {
	        document.getElementById('alle').style.display='none';
	    }
	}
</script>

</head>
<body <?php hybrid_attr( 'body' ); ?>>
	<preheader class="menu-container" <?php hybrid_attr( 'preheader' ); ?>>

		<div class="container preheader">
				<span>
				<a href="http://www.ledprofi.com/led-lampen-testsieger/">Testsieger</a> | <a href="http://www.ledprofi.com/versandkosten/">
				<?php global $woocommerce; $user_country = $woocommerce->customer->get_country( ); if ($user_country == 'DE') { ?>
				Gratis Versand in <img class="austria-flag" alt="Deutschland" src="http://www.ledprofi.com/wp-content/uploads/2015/05/germany-e1433844655177.jpg"> ab 59,00 €
				<?php }
				else { ?>
				Gratis Versand in <img class="austria-flag" alt="Österreich" src="http://www.ledprofi.com/wp-content/uploads/2015/05/Austria.png"> ab <?php global $versandkostengrenze; echo $versandkostengrenze ?>
				<?php } ?>
				</a> | <a href="http://www.ledprofi.com/bezahlmoeglichkeiten/">Sicherer Kauf auf Rechnung</a> | <a href="/geld-zurueck-garantie/">30 Tage Geld-Zurück-Garantie</a></span>
				<span style="float:right;">
				<strong>Hotline zum Ortstarif, Mo.-Fr. von 9:00-16:00 Uhr:</strong> <a href="tel:0043720884134">+43 (0) 720 884134</a>
				</span>
		</div>

	</preheader>
	<header>
		<div class="container">
	        <nav class="navbar no-border-radius xs-height75" id="main_navbar" role="navigation">
	          
	            <div class="navbar-header">
	              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar_id">
	              <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
	              </button>
<a class="navbar-brand" href="http://www.ledprofi.com" style="color:#FDBB30;">

<div class="left_navbar_brand"><?php if(is_front_page() ) {?><h1><?php }?><img style="max-height:74px;" alt="LED-Lampen vom LEDprofi" src="http://www.ledprofi.com/wp-content/uploads/ledprofi_2.png"/><?php if(is_front_page() ) {?></h1><?php }?></div>
</a>
<a href="/warenkorb/" class="mobile 
<?php global $woocommerce; $cart_content = $woocommerce->cart->cart_contents_count;
if ($cart_content == 0) {
	echo 'gray ';
	} ?>
btn btn-block btn-warning"><i style="font-size:42px" class="fa fa-shopping-cart"></i> <i class="fa fa-angle-down"></i>
<?php
global $woocommerce; $cart_content = $woocommerce->cart->cart_contents_count;
if ($cart_content>0) { ?>
<span class="
<?php 
$content_length = strlen($cart_content); if ($content_length >=2) { echo 'twofigures ';}
?>
cart_content">
	<?php echo $cart_content ?>	
</span>
<?php } ?></a>

	            </div>
	            <div class="collapse navbar-collapse" id="navbar_id">
	              <ul class="nav navbar-nav navbar-left">
	                  <li class="dropdown-grid">
	                      <a href="/gesamtes-sortiment/" class="dropdown-toggle birne">Gesamtes Sortiment</span></a>
	                  </li>
	                  <li class="divider"></li>
	                  	  <li class="dropdown-grid">
	                      <a href="/gesamtes-sortiment/?_sfm_retro-look=1" class="dropdown-toggle kerze">Retro-Look</span></a>
	                  </li>
	                  <li class="divider"></li>
	                  <li class="dropdown-grid">
	                      <a href="/spot-leds/" class="dropdown-toggle spot">Spot-LEDs</span></a>
	                	</li>
				 <li class="divider"></li>
	        				<!-- dropdown active -->
	        				<li class="dropdown-full no-border-radius no-shadow">
	        					<a data-toggle="dropdown" href="javascript:;" class="dropdown-toggle mehr"><span class="">Mehr</span><span class="caret"></span></a>
	        					<div class="dropdown-menu no-padding">
	        						<ul>
	        							<li class="col-sm-12 dropdown-header text-center" style="padding-bottom: 10px; border-bottom: 1px solid #555;color: #333; margin: 0 -1px; width: calc(100% + 2px);">
	        							<button style="position:absolute;
right:10px;" type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
	        							<div class="row mehr_header">
	        							<div style="border-right: 1px #FDBB30 dashed;" class="col-md-4">
	        							<h4>Spezielle Lampen</h4>
	        							<ul class="mehr_list">
	        							<a style="cursor:pointer;" href="http://www.ledprofi.com/gesamtes-sortiment/?_sfm_dimmbar=1"><li>Dimmfähige Lampen</li></a>
	        							<a href="http://www.ledprofi.com/led-birnen-e27-e14/?_sfm_form=Globe"><li>LED-Globes</li></a>
	        							<a href="http://www.ledprofi.com/led-g9/"><li>G9 Leuchtmittel</li></a>
	        							<a href="http://www.ledprofi.com/gesamtes-sortiment/"><li>Gesamtes Sortiment</li></a>
	        							</ul>
	        							</div>
	        							<div style="border-right: 1px #FDBB30 dashed;" class="col-md-4">
	        							<h4>Informationen</h4>
	        							<ul class="mehr_list">
	        							<a href="/newsletter/"><li>Newsletter</li></a>
	        							<a href="/versandkosten/"><li>Versand</li></a>
	        							<a href="http://www.ledprofi.com/bezahlmoeglichkeiten/"><li>Zahlungsmethoden</li></a>
	        							</ul>
	        							</div>
	        							<div class="col-md-4">
	        							<h4>Über uns</h4>
	        							<ul class="mehr_list">
	        							<a href="/ueber-uns/"><li>Team</li></a>
	        							<a href="/kontakt/"><li>Kontakt</li></a>
	        							</ul>
	        							</div>
	        							</div>

	        							</li>
	        						</ul>
	        					</div>
	        				</li>
	              </ul>
	              <ul class="nav navbar-nav navbar-right">
	                <!-- Right side navigation-->
	                <li style="padding:0 15px;padding-right:0px">
	                  <div class="btn-group btn-block" style="margin-top: 11px; margin-bottom: 11px;">
	                    <button type="button" class="
	                    <?php global $woocommerce; $cart_content = $woocommerce->cart->cart_contents_count;
	                    if ($cart_content == 0) {
	                    	echo 'gray ';
	                    	} ?>
	                    btn btn-block btn-warning dropdown-toggle" data-toggle="dropdown"><i style="font-size:42px" class="fa fa-shopping-cart"></i> <i class="fa fa-angle-down"></i>
	                    <?php
	                    global $woocommerce; $cart_content = $woocommerce->cart->cart_contents_count;
	                    if ($cart_content>0) { ?>
	                    <span class="
	                    <?php 
	                    $content_length = strlen($cart_content); if ($content_length >=2) { echo 'twofigures ';}
	                    ?>
	                    cart_content">
	                    	<?php echo $cart_content ?>	
	                    </span>
	                   <?php } ?></button>

	                    <div class="dropdown-menu">
	                    	<?php woocommerce_get_template( 'cart/mini-cart.php', $args ); ?>
	                    </ul>
	                  </div>
	                  
	                </li>
	              </ul>
	          </div>
	        </nav>

	    </div>
	   </header>

	<main <?php hybrid_attr( 'content' ); ?>>
